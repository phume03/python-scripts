import turtle

window = turtle.Screen()
window.title("Lines 3")
window.setup(width=400, height=400, startx=None, starty=None)
window.mode('logo')

# ========= given code ===============
def drawLine(xFrom, yFrom, xTo, yTo, color, myPen):
   myPen.penup()
   myPen.color(color)
   myPen.goto(xFrom, yFrom)
   myPen.pendown()
   myPen.goto(xTo, yTo)
 
def drawCircle(x, y, radius, color, myPen):
   myPen.setheading(0)
   myPen.penup()
   myPen.color(color)
   myPen.goto(x, y-radius)
   myPen.pendown()
   myPen.circle(radius)
 
def drawSquare(x, y, size, color, myPen):
   myPen.setheading(0)
   myPen.penup()
   myPen.goto(x,y)
   myPen.color(color)
   myPen.fillcolor(color)
   myPen.pendown()
   myPen.begin_fill()
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.end_fill()

myPen = turtle.Turtle()
myPen.hideturtle()
myPen.speed(500)

step = 15
for x in range (0,2):
  if x==0:
    yval = -40
    yval2 = -120
  else:
    yval = 40
    yval2 = 120
    
  drawLine(-90, yval, 90, yval, "purple", myPen)
  for y in range(1, 20):
      if y%2==0:
        drawLine(-1 * y * step, yval2, 0, 0, "black", myPen)
      else:
        drawLine(1 * y * step, yval2, 0, 0, "black", myPen)


# ========= given code ===============
window.exitonclick()
