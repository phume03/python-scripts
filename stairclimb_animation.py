import os
import time

start = 0
end = 20

def person(moveUp, moveRight):
  print "\n" * moveUp
  print (" " * moveRight) + "o"
  print (" " * moveRight) + "|"
  print (" " * moveRight) + "^"
  print "\n" * (end - moveUp + 1)
  print "Metrics: " + str(moveUp) + " " + str(moveRight)

def animate():
  distanceFromTop = end
  distanceFromLeftSide = start
  stepSize = 2
  stepUp = -1 * stepSize
  stepRight = 1 * stepSize
  stepSpace = False
  while True:
      person(distanceFromTop, distanceFromLeftSide)

      if stepSpace == True:
        distanceFromLeftSide += stepRight
        stepSpace = False
      else:
        distanceFromTop += stepUp
        stepSpace = True

      if distanceFromLeftSide>=end:
          stepRight = -1 * stepSize

      if distanceFromTop <= start:
          stepUp = 1 * stepSize

      if distanceFromLeftSide<=start:
          stepRight = 1 * stepSize

      if distanceFromTop >= end:
          stepUp = -1 * stepSize

      if distanceFromTop == start and distanceFromLeftSide == end:
          stepSpace = True

      if distanceFromTop == end and distanceFromLeftSide == start:
          stepSpace = False

      time.sleep(0.25)
      os.system('clear')


animate()
