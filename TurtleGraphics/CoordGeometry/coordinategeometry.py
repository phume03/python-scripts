#! /usr/bin/env python
# @author: Phumelela Mdluli
# @date: 06 June 2023
# @name: Parabolic Curve
# @description: this is an improvement on my first attempt to create a program that produces coordinate geometry artwork.
# @python version: 3.8
import math, turtle
print("""Coordinate Geometry: Cartesian Grid Artwork""")

directions = ["E","S","W","N"] # offset by 90 from north, N
step_divisions = 25
venus = turtle.Screen()
venus.title("Basic Lines Function")
venus.setup(width=600, height=600, startx=None, starty=None)
venus.mode('logo')
venus.delay(0.25)
width = venus.window_width()
height = venus.window_height()

schildpad = turtle.Turtle()
schildpad.hideturtle()

def drawAxis(pen):
  for x in directions:
    pen.penup()
    pen.goto(0,0)
    pen.pendown()
    index_of_x = directions.index(x)
    sector = index_of_x + 1
    angle = 90*(sector)
    pen.setheading(angle)
    if (sector%2 == 0):
      pen.forward(height/2.25)
    else:
      pen.forward(width/2.25)
  return None

def basicLines(pen):
  lines = int(width/step_divisions)
  height_step = (((height/2.25)*2)/lines)
  width_step = (((width/2.25)*2)/lines)
  for x in directions:
    sector = directions.index(x)+1
    if (sector%2 !=0 ):
      x1 = ((width/2.25)*((sector-2)*-1))
      y1 = 0
    else:
      x1 = 0
      y1 = ((height/2.25)*(sector-3))
    for y in range(0, lines+1):
      pen.penup()
      pen.goto(x1, y1)
      if (sector%2 != 0):
        y2 = (height/2.25) - (height_step  * y)
        x2= 0
      else:  
        y2 = 0
        x2=(width/2.25) - (width_step  * y)
      pen.pendown()
      pen.goto(x2,y2)
      pen.penup()
      pass
    pass
  return None

drawAxis(schildpad)
basicLines(schildpad)
venus.exitonclick()
