#! /usr/bin/env python
# @author: Phumelela Mdluli
# @date: 05 June 2023
# @name: Parabolic Curve
# @description: parabolic curve artwork.
# @python version: 3.8
import turtle

print("""Coordinate Geometry: Parabolic Curve""")
jupiter = turtle.Screen()
jupiter.title("Parabolic Curve")
jupiter.setup(width=600, height=600, startx=None, starty=None)
jupiter.mode('logo')
jupiter.delay(0.25)

# tics are the total number of origin points, thus lines along a single axis
tics = 50
# step_size translates to the number of pixels between two origin points along an axis
step_size = 5

# Create a turtle
tataruga = turtle.Turtle()
tataruga.hideturtle()
tataruga.color("black")

# Draw Quadrant positive-x positive-y
for tic in range(0, tics+1):
    xA = 0
    yA = (tics - tic) * step_size
    xB = tic * step_size
    yB = 0
    tataruga.penup()
    tataruga.goto(xA, yA)
    tataruga.pendown()
    tataruga.goto(xB, yB)
 
# Draw Quadrant positive-x negative-y
for tic in range(0, tics+1):
    xA = (tics - tic) * step_size
    yA = 0
    xB = 0
    yB = tic * step_size * - 1
    tataruga.penup()
    tataruga.goto(xA, yA)
    tataruga.pendown()
    tataruga.goto(xB, yB)
 
# Draw Quadrant negative-x negative-y
for tic in range(0, tics+1):
    xA = 0
    yA = (tics - tic) * step_size * -1
    xB = tic * step_size * -1
    yB = 0
    tataruga.penup()
    tataruga.goto(xA, yA)
    tataruga.pendown()
    tataruga.goto(xB, yB)

# Draw Quadrant negative-x positive-y 
for tic in range(0, tics+1):
    xA = (tics - tic) * step_size * -1
    yA = 0
    xB = 0
    yB = tic * step_size
    tataruga.penup()
    tataruga.goto(xA, yA)
    tataruga.pendown()
    tataruga.goto(xB, yB)

jupiter.exitonclick()
