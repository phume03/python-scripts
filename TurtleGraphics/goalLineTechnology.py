#! /usr/bin/env python
# @author: Phumelela Mdluli
# @date: 05 June 2023
# @name: Goal-Line Techmology
# @description: implementation of goal-line technology in the MTN Premier League, in Eswatini.
# @python version: 3.8
import turtle
from random import randint

print("""Premier League of Eswatini (PLS)""")
pls_teams = ["Denver Sundowns", "Green Mamba", "Malanti Chiefs", "Manzini Sea Birds", "Manzini Wanderers", "Mbabane Highlanders", "Mbabane Swallows", "Milling Hotspurs", "Moneni Pirates", "Rangers", "Royal Leopards", "Tambankulu Callies", "Tambuti", "Tinyosi", "Vovovo", "Young Buffaloes"]
team_one_index = randint(0, len(pls_teams)-1)
team_one = pls_teams[team_one_index]
goals = 0
mars = turtle.Screen()
mars.title("MTN Premier League")
mars.setup(width=800, height=600, startx=None, starty=None)
mars.mode('logo')
mars.delay(0.25)
mars.tracer(0)
mars.bgcolor("#236127")

fudvu = turtle.Turtle()

def strike():
    global goals
    fudvu.reset()
    fudvu.speed(0) # speed 1 (slow) to 10 (fast), and 0 (fastest)
    if fudvu.isvisible():
        fudvu.hideturtle()
    if fudvu.isdown():
        fudvu.penup()
	
    # reset screen origin
    start_x = (mars.window_width()/2 * -1)
    start_y = (mars.window_height()/2)
    
    # draw field
    goal_line_start_x = start_x + (mars.window_width()/4)
    goal_line_start_y = start_y
	
    fudvu.goto(goal_line_start_x, goal_line_start_y)
    fudvu.pencolor("white")
    fudvu.pensize(10)
    fudvu.setheading(180)
    fudvu.pendown()
    fudvu.fd(mars.window_height())
    fudvu.penup()
	
    # add goal posts
    goal_post_y_first = mars.window_height()/4
    goal_post_y_second = goal_post_y_first * -1
    fudvu.goto(goal_line_start_x, goal_post_y_first)
    fudvu.pendown()
    fudvu.dot(20, "white")
    fudvu.penup()
	
    fudvu.goto(goal_line_start_x, goal_post_y_second)
    fudvu.pendown()
    fudvu.dot(20, "white")
    fudvu.penup()
	
    # randomly add ball
    field_length = mars.window_width()
    ball_radius = 30
    ball_x_pos = randint(start_x, start_x + field_length)
    ball_y_pos = randint(start_y * -1, start_y)
    fudvu.goto(ball_x_pos,ball_y_pos)
    fudvu.pensize(1)
    fudvu.pencolor("black")
    fudvu.fillcolor("red")
    fudvu.pendown()
    fudvu.begin_fill()
    fudvu.circle(ball_radius)
    fudvu.end_fill()
    fudvu.penup()

    fudvu.goto(0, 40)
    fudvu.pensize(1)    

    #Check if the ball is behing the goal line and between the goal posts
    if ball_x_pos + ball_radius <= goal_line_start_x and ball_y_pos - ball_radius >= goal_post_y_second and ball_y_pos + ball_radius <= goal_post_y_first:
        goals += 1
        fudvu.pencolor("#E50201")
        fudvu.write(f"Goal!", font=('Arial', 28, 'bold'))
    else:
        fudvu.pencolor("#FF8080")
        fudvu.write(f"No Goal", font=('Arial', 28, 'bold'))

    fudvu.goto(goal_line_start_x+10, start_y-40)
    fudvu.pensize(1)
    fudvu.pencolor("black")
    show_score = f"{team_one:20}: {goals:3}"
    fudvu.write(show_score, font=('Arial', 14, 'normal'))
    #print(show_score)
    mars.update()
    mars.ontimer(strike,1000)
    
strike()
mars.exitonclick()
