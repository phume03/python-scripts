#! /usr/bin/env python
# @author: Phumelela Mdluli
# @date: 05 June 2023
# @name: Clock
# @description: using the Python turtle library and the datetime library to create a program that displays the current time as an anlogue clock.
# @python version: 3.8
import turtle, datetime

print("""Analogue Clock""")
pluto = turtle.Screen()
pluto.title("Python Turtle Clock")
pluto.setup(width=600, height=600, startx=None, starty=None)
pluto.mode('logo')
pluto.delay(0.25)
pluto.tracer(0)
pluto.bgcolor("black")

# Create a turtle
tortuga = turtle.Turtle()

def tellTime():
    pluto.reset()
    tortuga.hideturtle()
    
    # Create Clock Face
    tortuga.pencolor("purple")
    tortuga.fillcolor("white")
    tortuga.pensize(10)
    tortuga.speed(0) # speed 1 (slow) to 10 (fast), and 0 (fastest)
    tortuga.penup()
    tortuga.goto(200,0)
    tortuga.pendown()
    tortuga.begin_fill()
    tortuga.circle(200)
    tortuga.end_fill()

    # Face Graduations
    tortuga.pencolor("black")
    tortuga.pensize(2)
    for x in range(0,360,30):
        tortuga.penup()
        tortuga.setheading(x)
        tortuga.goto(0,0)
        tortuga.forward(160)
        tortuga.pendown()
        tortuga.forward(35)
        
    tortuga.pencolor("gray")
    tortuga.pensize(1)
    for x in range(0,360,6):
        tortuga.penup()
        tortuga.setheading(x)
        tortuga.goto(0,0)
        tortuga.forward(180)
        tortuga.pendown()
        tortuga.forward(15)

    # Add Clock Hands
    currentMinute = datetime.datetime.now().minute
    currentHour = datetime.datetime.now().hour
    currentSecond = datetime.datetime.now().second

    tortuga.pencolor("red")
    tortuga.pensize(5)
    tortuga.penup()
    tortuga.goto(0,0)
    tortuga.setheading((currentHour * 360 / 12) + (currentMinute*30/60)) # Point to the top - 12 o'clock
    tortuga.pendown()
    tortuga.forward(100)

    tortuga.pencolor("orange")
    tortuga.pensize(3)
    tortuga.penup()
    tortuga.goto(0,0)
    tortuga.setheading(currentMinute*360/60) # Point to the top - 0 minute
    tortuga.pendown()
    tortuga.forward(150)

    tortuga.pencolor("yellow")
    tortuga.pensize(1)
    tortuga.penup()
    tortuga.goto(0,0)
    tortuga.setheading(currentSecond*360/60) # Point to the top - 0 second
    tortuga.pendown()
    tortuga.forward(150)

    pluto.update()    
    pluto.ontimer(tellTime,100)
    
tellTime()
pluto.exitonclick()
