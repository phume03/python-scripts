# PYTHON SCRIPTS #
-----

## Use Instructions ##
The script requires python2.X and has not been upgraded for use on later versions of python. These are linux/unix install instructions.

### Python 2.x (required for most scripts) ###

    sudo apt-get install python2.x

*Note:* that pip3 will require Python 3.4 and above.

### Python 3.5 ###
    sudo apt-get update

    sudo apt-get install python3.5 python3.5-dev python3.5-doc python3.5-examples python3.5-venv

*Note:* python3.5 is required by some packages.

### Python Dependencies e.g PIP (required to install and run other packages) ###
    sudo apt-get install python3-pip python3-wheel python-pil

### Python Packages: XLRD, XWLT ###
    sudo pip3 install --upgrade pip

Confirm that the right version of pip has been installed, and it is attached or pointing to the python3.5 (or later) interpreter and not any other version of python, by running the following command:

    sudo pip3 -V 

This matters because some packages will not run on a older versions of pip.

    sudo pip3 install xlrd xlwt 

OR

    sudo python3.5 -m pip install xlrd xlwt 

Remember to check the project dependencies. Scripts with missing dependencies will break, so pay attention to the log message, and resolve it accordingly.

----

You may wish to use other python tools to perform some of the operations completed by my scripts. Feel free to do further research on python and download, install, and employ the necessary tools. You may find this more rewarding than doing things exactly as I have.

----

## SCRIPTS/MODULES ##

*die roll - simple die roll script

*rice krispies - simulation of cereal boxes and collecting a secret prize

*project euler solutions - [project euler] (https://www.projecteuler.net) is a website for mathematics enthusiasts, these are programmatic solutions to 11 of their problems.

* python tutorial - created before all the other scripts, should help users to learn python from scratch.

* sorting algorithms - various sorting algorithms implemented in python.

* excel refine - my brain child from college. A simple python script to automate performing specific edits on an excel file, using simple data flow algorithms. I wrote the code so that you do not have to. Contributions and user feedback are welcome! I strongly advise that you read your excel file before using this script. Data input is performed using the xlrd package found in the python package index, and data output is performed using the xlwt package also found in the python package index.

### Turtle Graphics ###

"Turtles" is how graphics were introduced to me in [Computer Science](https://docs.python.org/3.8/library/turtle.html) using [Jython](https://www.jython.org/) or more accurately the student IDE implementation of it known as [Jython Environment for Students (JES)](https://github.com/gatech-csl/jes). Python contains a library/module called the turtle which is an extended reimplementation of the same-named module from the Python standard distribution up to version Python 2.5. The following are sample applications of turtles in python.

#### Coordinate Geometry ####

###### Cartesian Grid Artwork ######

I wrote a script to draw straight lines from the four axis of a cartesian grid. It used pythagoras and was approximate in what it was trying to do. I have simplified the work, it now executes a simpler draw no different from using pen and paper. It is more accurate and looks interesting. This was the precursor to the parabolic curve.

###### Parabolic Curve ######

Some guy blogged it well [here](https://mathcraft.wonderhowto.com/how-to/create-parabolic-curves-using-straight-lines-0131301/). It skips the complex explanation from my highschool mathematics classes -- taught by a Mr. Richard Alexanderand much enjoyed. I will say this, when it comes to the particular bit on coordinate geometry for which I created a script, I missed the lesson and only recall a piece of artwork pop-up in the classroom block by an IB mathematics student. This may be the same student who went on to do outstanding work in AIDs research abroad, she is Swati. I cannot recall now - he whole class was one of geniuses. Anyway, I will share the pictures for you all to appreciate the basic process when done on pen and paper. It all requires a little bit more thought and time to do on a computer (infact, it requires time to do on paper that one might suspect a student of mathematics at any level should not have... but alas, art has stolen many hearts and the most successful artists stole a moment from some high level mathematics/physics concept to really SHOW the world what it is they were learning and that's love).

Below are picture instructions on the construction of parabolic curves, I hope you appreciate them:

1. Start with a graph of the cartesian grid, choosing/using only one quadrant (positive x and y axis is fine). 

![Cartesian Grid](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.pencil-steps01.jpg "Start/Step 1") 

2. Draw a single straight line from the point x=0 and y=max to a single point x=step-size and y=0. 

![Step 02](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.pencil-steps02.jpg "Step 2")

3. Draw the next **straight line** (important) from a slightly different origin point x=0 and y=max-step-size to a single point x=(step-size x 2) and y=0. 

![Step 03](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.pencil-steps03.jpg "Step 3")

4. And the next (line) maintaining the x value of the origin and decreasing it's y value by a single unit of the step-size, and increasing the destination x value by a single unit of the step size as shown and maintaining it's y value. Do this until your origin point is x=0 and y=step-size, and the destination point is x=max and y=0.
 
![Step 04](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.pencil-steps04.jpg "Step 4")

In python code this might look like:

````
for i in range(0,11):
    xFrom = 0
    yFrom = (10-i) * 20
    xTo = i * 20
    yTo = 0
    myPen.penup()
    myPen.goto(xFrom,yFrom)
    myPen.pendown()
    myPen.goto(xTo,yTo)
````

5. For now, appreciate what you have done in these first few steps. Then think about how it would look with a bigger maximum value on the x and y axis, a smaller step-size, or both a bigger cartesian graph and smaller step sizes. Try it, and see if any of your attemps look like the picture in this step. 

![Step 05](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.pencil-steps05.jpg "Step 5")

6. Now, imagine extending your graph or grid to the other three quadrants, or negative x and y axises. What would your artwork look like if you repeated the steps above? See the following picture for a teaser. 

![Teaser All Quadrants](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.pencil-steps06.jpg "Teaser All Quadrants")

7. Despite having thought about this quite a bit, I had never thought of the following, what if you drew the axis, instead of at right-angles to each other, at some acute angle, and then joined the two sides on the opposite ends of the apex/vertex created by the joined edges with another straight-line. For aesthetics, aim for a line that is an equal length to the other two edges/lines to make an equilateral triangle. 

![Equilateral Triangle](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.pencil-steps06b.jpg "Equilateral Triangle")

8. Then, repeat the steps as before. 

![Cool Geometric Shape](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.pencil-steps06c.jpg "Cool Geometric Shape")

9. Can you see a familiar shape? 

![Cool Geometric Shape Highlighted](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.pencil-steps06d.jpg "Highlighted Geometric Shape")

10. If you go over what you just learned with different colors, you can get some interesting things to look at (color always dazzles). 

![Colored](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.positive quadrants.jpg "Colored")

11. Do you see what I mean? The fella who created the original math hack blog meant serious business and his business will probably have inspired some serious business from me as well creating replicas of this geometric work using a computer/turtles in python - ALL FOR THE LOVE OF ART & MATHS! 

![TRUE LOVE](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.love.jpg "True Love")

12. Here is a variation of all this work using other materials either than pen/pencil and paper. 

![TRIANGLE REPEATED](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.application1.jpg "Triangle Repeated")

13. And, another variation using the same medium (art language) and rotating the triangle rather than repeating it. 

![SPIN](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.application2.jpg "Spin")

14. These are computer generated, I suppose the fella was inspired. I basically duplicated his blog by using his images -- anyhow, if you have not gone there, love this. Just look. 

![Computer Generated 01](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.complex.jpg "Computer Generated 01")

15. One more. Just look. I have stolen some fun from doing this ourselves. It should take as much time as it takes using pen/pencil and paper to figure out what your eyes are seeing and to replicate it on a PC. 

![Computer Generated 02](./TurtleGraphics/CoordGeometry/create-parabolic-curves-using-straight-lines.complex2.jpg "Computer Generated 02"). 



Once you have appreciated all of that, run some of my scripts to see what I was able to reproduce. My starting point was the [python challenge](https://www.101computing.net/python-turtle-challenge/) on 101Computing \[dot\] net.



#### Date Time ####

##### Analogue Clock #####

A simple analogue clock using turtle graphics logic.



#### Miscellenous #### 

##### Goal Line Technology #####

An implementation of the goal line technology used in soccer, according to [FIFA](https://www.fifa.com/technical/football-technology/football-technologies-and-innovations-at-the-fifa-world-cup-2022/goal-line-technology) as of the world-cup of 2014 in Brazil (won by Germany). Goal-line technology is a technical means of instantly determining whether "**the whole of the ball** has crossed the goal-line". The technology must provide a clear indicator as to whether a ball has **fully crossed** the goal line. This helps referees to make a final decision.



#### Turtle Cheat Sheet ####

I am glad that you chose to read my reference. All this information can also be found in the [python help](https://docs.python.org/3.8/library/turtle.html#turtle.color).

To work with turtles one has to import the module as follows:

````
from turtle import *
````


###### Turtle World/Canvas ######

A turtle is **like** a pen. To use a pen one needs a canvas, we call this a world in CS. So, to begin one creates a world like so:

````
earth = turtle.Screen()
````

Now your turtle can run around and "paint the world" as you see fit - moving in straight lines, squiggly lines, curvy lines, etc to create a world that suites your imagination.

| Screen Methods | |
|----------------|-|
| Screen() | creates a new turtle world/canvas. |
| setup() | allows you to set some defaults for the world. Some common variables are; width, height, startx, starty. All are integer values |
| title() | allows you to give a title to the window object that will be displayed. Takes a string parameter |
| bye() | |
| exitonclick() | sets a window event to close the window object when a user clicks in the view area. |
| mode() | |
| window_height() | |
| window_width() | |
| bgcolor() | |
| bgpic() | |
| clear() or clearscreen() | |
| reset() or resetscreen() | |
| setworldcoordinates() | |

| Screen Events | |
|---------------|-|
| listen() | |
| onkey() | |
| onclick() or onscreenclick() | |
| ontimer() | |

| Animation Control | |
|-------------------|-|
| delay() | |
| tracer() | |
| update() | |


###### Turtle (Pen) ######

| Turtle Methods | |
|----------------|-|
| Turtle() | creates a new turtle or pen. More than one can be created in a world. |
| color() | |
| pencolor() | |
| fillcolor() | |
| showturtle() or st() | |
| hideturtle() or ht() | |
| isvisible() | |

| Draw | |
|------|-|
| pendown() or pd() or down() | |
| penup() or pu() or up() | |
| pensize() or width() | |
| pen() | |
| isdown() | |
| forward() or fd() | |
| backward() or bk() or back() | |
| right() or rt() | |
| left() or lt() | |
| goto() or setpos() or setposition() | |
| setx() | |
| sety() | |
| setheading() or seth() | |
| home() | |
| circle() | |
| dot() | |
| stamp() | |
| clearstamp() | |
| clearstamps() | |
| undo() | |
| speed() | |
| write() | |

| Events | |
|--------|-|
| onclick() | |
| onrelease() | |
| ondrag() | |
| mainloop() or done() | |

Tell Turtle's State
-------------------
position() | post()
towards()
xcor()
ycor()
heading()
distance()

Setting and Measurement
-----------------------
degrees()
radians()

Filling
-------
fill()
begin_fill()
end_fill()

Appearance
----------
shape()
resizemode()
shapesize() | turtlesize()
settiltangle()
tiltangle()
tilt()

Special Turtle Methods
----------------------
begin_poly()
end_poly()
get_poly()
clone()
getturtle() | getpen()
getscreen()
setundobuffer()
undobufferentries()
tracer()
window_width()
window_height()

----

## Useful Notes ##
[Official Python Documentation] (https://www.python.org/doc/), here you will find useful resources for using python, including an introduction to OOP python.

[Python Package Index] (https://pypi.python.org/pypi), here you will find an array of packages to use with your projects. xlwt and xlrd packages can be downloaded from here.

[OS Module] (https://docs.python.org/2/library/os.path.html), follow this link to learn more about the OS module.

Some of the dependent modules have been provided in the directory *'/dependencies'*. However, I suggest that you follow the **python package index** above to find the documentation for the modules, and updated versions of the modules. The *'test files'* provided were randomly generated, please do not expect them to be accurate e.g. if Jane is listed as a male.  Generate your own *'test files'* or simply use the file you actually need to edit.



