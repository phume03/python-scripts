import turtle

window = turtle.Screen()
window.title("Black Dot")
window.setup(width=600, height=600, startx=None, starty=None)
window.mode('logo')

# ========= given code ===============
def drawLine(xFrom, yFrom, xTo, yTo, color, myPen):
   myPen.penup()
   myPen.color(color)
   myPen.goto(xFrom, yFrom)
   myPen.pendown()
   myPen.goto(xTo, yTo)
 
def drawCircle(x, y, radius, color, myPen):
   myPen.setheading(0)
   myPen.penup()
   myPen.color(color)
   myPen.fillcolor(color)
   myPen.goto(x, y-radius)
   myPen.pendown()
   myPen.begin_fill()
   myPen.circle(radius)
   myPen.end_fill()
 
def drawSquare(x, y, size, color, myPen):
   myPen.setheading(0)
   myPen.penup()
   myPen.goto(x,y)
   myPen.color(color)
   myPen.fillcolor(color)
   myPen.pendown()
   myPen.begin_fill()
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.end_fill()

myPen = turtle.Turtle()
myPen.hideturtle()
myPen.speed(500)

myPen.pensize(10)

drawSquare(300, 0, 300, '#cccccc', myPen)
drawSquare(290, 10, 280, "black", myPen)

step=50
for y in range (0,7):
  drawLine(300, y*step, 0, y*step, '#cccccc', myPen)

for x in range(0,7):
  drawLine(x*step, 300, x*step, 0, '#cccccc', myPen)

myPen.pensize(.5)
size=10
for y in range (1,6):
  for x in range(1,6):
    drawCircle((y*step)+size, (x*step)+size, size, "white", myPen)



# ========= given code ===============
window.exitonclick()
