#! /usr/bin/env python
# @author: Phumelela Mdluli
# @description: Sorting algorithms written in python. Default array size is 10, and the minimum array value is 0 while the maximum is 100.
import random as rand
import array as arr
Array_Size = 10
Unsorted_Array = arr.array('i',())

for index in range(0,Array_Size):
  Unsorted_Array.append(rand.randint(0,100))

enter=0

# @description: bubble sort is the most basic sorting algorithm used by human beings everyday to sort numbered items. It works by comparison and moves the larger numbers to the end of the given list.
def bubblesort(Array1):
  for i in range(0,len(Array1)-1):
    for j in range(i+1,len(Array1)):
      if (Array1[i]>Array1[j]):
        temp=Array1[i]
        Array1[i]=Array1[j]
        Array1[j]=temp
  return Array1
 
# @description: merge sort falls under the divide and conquer types of algorithms. It works by dividing the array into simpler parts, sorting them and combines the sorted smaller pieces to produce a sorted whole array. My implementation is recursive.
def mergesort(Array1):
  ArraySize=len(Array1)
  if (ArraySize>=2):
    CutOff=int(ArraySize/2)
    A=mergesort(Array1[0:CutOff])
    B=mergesort(Array1[CutOff:ArraySize])
 
    ArrSize=(len(A)+len(B))
    Array1=arr.array('i',[])
    indexA=0
    indexB=0
    for i in range(0,ArrSize):
      if (indexA>=len(A)):
        Array1.append(B[indexB])
        indexB=indexB+1

      elif (indexB>=len(B)):
        Array1.append(A[indexA])
        indexA=indexA+1

      else:
        if (A[indexA]<B[indexB]):
          Array1.append(A[indexA])
          indexA=indexA+1
    
        elif (A[indexA]>=B[indexB]):
          Array1.append(B[indexB])
          indexB=indexB+1

        else:
          print "Error: incorrect sequence!"
          break

  return Array1  


def main():
  print "\nPython 2 Sorting Algorithms"
  print "Given the following array, how would you like to sort the given numbers (N)?"
  global Unsorted_Array
  Array_toSort=arr.array('i',Unsorted_Array.tolist())
  print "Unsorted Array: ",Unsorted_Array.tolist()
  sort_array=True
  while (sort_array!=False):
    print "These are the basic sorting algorithms\n1) Bubble Sort\n2) Merge Sort\n3) Quick Sort (NA) \n4) Bucket Sort (NA) \n5) Exit"
    response=raw_input("Enter your choice:")
    response=int(response)
    if (response==1):
      print "Unsorted Array: ",Array_toSort.tolist()
      Sorted=bubblesort(Array_toSort)
      print "Sorted Array (Bubble Sort): ",Sorted.tolist(),"\n"

    elif (response==2):
      print "Unsorted Array: ",Array_toSort.tolist()
      Sorted=mergesort(Array_toSort)
      print "Sorted Array (Merge Sort): ",Sorted.tolist(),"\n"

    elif (response==3):
      print "Quick sort has not been implemented yet!"

    elif (response==4):
      print "Bucket sort has not been implemented yet!"

    elif (response==5):
      print "Exiting!"
      sort_array=False

    else:
      print "Error: Unrecognized input, exiting!"
      sort_array=False

main()
