import turtle

window = turtle.Screen()
window.title("Mobius Strip")
window.setup(width=400, height=400, startx=None, starty=None)
window.mode('logo')

# ========= given code ===============
def drawLine(xFrom, yFrom, xTo, yTo, color, myPen):
   myPen.penup()
   myPen.color(color)
   myPen.goto(xFrom, yFrom)
   myPen.pendown()
   myPen.goto(xTo, yTo)
 
def drawCircle(x, y, radius, color, myPen):
   myPen.setheading(0)
   myPen.penup()
   myPen.color(color)
   myPen.goto(x, y-radius)
   myPen.pendown()
   myPen.circle(radius)
 
def drawSquare(x, y, size, color, myPen):
   myPen.setheading(0)
   myPen.penup()
   myPen.goto(x,y)
   myPen.color(color)
   myPen.fillcolor(color)
   myPen.pendown()
   myPen.begin_fill()
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.end_fill()

myPen = turtle.Turtle()
myPen.hideturtle()
myPen.speed(500)

drawLine(-90, -100, 100, -100, "black", myPen)
drawLine(-100, -90, 80, -90, "black", myPen)
drawLine(-80, -80, 70, -80, "black", myPen)

drawLine(-90, -100, -100, -90, "black", myPen)

drawLine(-100, -90, 0, 100, "black", myPen)
drawLine(-80, -80, 20, 100, "black", myPen)
drawLine(-70, -80, 20, 80, "black", myPen)

drawLine(0, 100, 20, 100, "black", myPen)

drawLine(20, 100, 110, -90, "black", myPen)
drawLine(20, 80, 100, -100, "black", myPen)
drawLine(10, 60, 80, -90, "black", myPen)

drawLine(100, -100, 110, -90, "black", myPen)


# ========= given code ===============
window.exitonclick()
