import os
import time

def person(moveUp, moveRight):
  print "\n" * moveUp
  print (" " * moveRight) + "o"
  print (" " * moveRight) + "|"
  print (" " * moveRight) + "^"

def animate():
  start = 0
  end = 20
  distanceFromTop = end
  distanceFromLeftSide = start
  stepSize = 1
  stepUp = -1 * stepSize
  stepRight = 1 * stepSize
  moveRight = True
  moveUp = False
  while True:
      person(distanceFromTop, distanceFromLeftSide)
      
      if moveRight == True:
        distanceFromLeftSide += stepRight

      if moveUp == True:
        distanceFromTop += stepUp

      if distanceFromLeftSide == end:
          stepRight = -1 * stepSize

      if distanceFromTop == start:
          stepUp = 1 * stepSize

      if distanceFromLeftSide == start:
          stepRight = 1 * stepSize

      if distanceFromTop == end:
          stepUp = -1 * stepSize

      if (distanceFromLeftSide == end and distanceFromTop == end) or (distanceFromLeftSide == start and distanceFromTop == start):
          moveUp = True
          moveRight = False
      if (distanceFromLeftSide == end and distanceFromTop == start) or (distanceFromLeftSide == start and distanceFromTop == end):
          moveUp = False
          moveRight = True
     
      time.sleep(0.05)
      os.system('clear')


animate()
