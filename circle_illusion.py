import turtle

window = turtle.Screen()
window.title("Circles")
window.setup(width=400, height=400, startx=None, starty=None)
window.mode('logo')

# ========= given code ===============
def drawLine(xFrom, yFrom, xTo, yTo, color, myPen):
   myPen.penup()
   myPen.color(color)
   myPen.goto(xFrom, yFrom)
   myPen.pendown()
   myPen.goto(xTo, yTo)
 
def drawCircle(x, y, radius, color, myPen):
   myPen.setheading(0)
   myPen.penup()
   myPen.color(color)
   myPen.fillcolor(color)
   myPen.goto(x, y-radius)
   myPen.pendown()
   myPen.begin_fill()
   myPen.circle(radius)
   myPen.end_fill()
 
def drawSquare(x, y, size, color, myPen):
   myPen.setheading(0)
   myPen.penup()
   myPen.goto(x,y)
   myPen.color(color)
   myPen.fillcolor(color)
   myPen.pendown()
   myPen.begin_fill()
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.end_fill()

myPen = turtle.Turtle()
myPen.hideturtle()
myPen.speed(500)

for x in range (0,2):
  for y in range(0,5):
    if y==0:
      if x==0:
        drawCircle(-90, 0, 20, "purple", myPen)
      else:
        drawCircle(90, 0, 20, "purple", myPen)
    elif y==1:
      if (x==0):
        drawCircle(-90, 70, 35, "black", myPen)
      else:
        drawCircle(90, 70, 5, "black", myPen)
    elif y==2:
      if (x==0):
        drawCircle(-90, -70, 35, "black", myPen)
      else:
        drawCircle(90, -70, 5, "black", myPen)
    elif y==3:
      if (x==0):
        drawCircle(-160, 0, 35, "black", myPen)
      else:
        drawCircle(20, 0, 5, "black", myPen)
    else:
      if (x==0):
        drawCircle(-20, 0, 35, "black", myPen)
      else:
        drawCircle(160, 0, 5, "black", myPen)

# ========= given code ===============
window.exitonclick()
