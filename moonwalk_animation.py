import os
import time

def person(moveDown, moveRight, step):
  print "\n" * moveDown
  if (step == 1):
    figure1(moveRight)
  elif (step == 2):
    figure2(moveRight)
  elif (step == 3):
    figure3(moveRight)
  elif (step == 4):
    figure4(moveRight)
  elif (step == 5):
    figure5(moveRight)
  elif (step == 6):
    figure6(moveRight)
  elif (step == 7):
    figure7(moveRight)
  elif (step == 8):
    figure8(moveRight)
  else:
    figure9(moveRight)

def figure1(moveRight):
  print (" " * moveRight) + "o"
  print (" " * moveRight) + "|"
  print (" " * moveRight) + "|"
  print (" " * moveRight) + "|"

def figure2(moveRight):
  print (" " * moveRight) + "o"
  print (" " * moveRight) + "|"
  print (" " * moveRight) + "|"
  print (" " * moveRight) + "|\\"

def figure3(moveRight):
  print (" " * moveRight) + "o"
  print (" " * moveRight) + "|\\"
  print (" " * moveRight) + "|"
  print (" " * moveRight) + "|\\"

def figure4(moveRight):
  print (" " * moveRight) + " o"
  print (" " * moveRight) + " |\\"
  print (" " * moveRight) + " |"
  print (" " * moveRight) + "/\\"

def figure5(moveRight):
  print (" " * moveRight) + " o"
  print (" " * moveRight) + " |\\"
  print (" " * moveRight) + " |"
  print (" " * moveRight) + "/|"

def figure6(moveRight):
  print (" " * moveRight) + " o"
  print (" " * moveRight) + " |"
  print (" " * moveRight) + " |"
  print (" " * moveRight) + "/|"

def figure7(moveRight):
  print (" " * moveRight) + " o"
  print (" " * moveRight) + "/|"
  print (" " * moveRight) + " |"
  print (" " * moveRight) + "/|"

def figure8(moveRight):
  print (" " * moveRight) + " o"
  print (" " * moveRight) + "/|"
  print (" " * moveRight) + " |"
  print (" " * moveRight) + " |"

def figure9(moveRight):
  print (" " * moveRight) + " o"
  print (" " * moveRight) + " |"
  print (" " * moveRight) + " |"
  print (" " * moveRight) + " |"

def animate():
  start = 50
  distanceFromTop = 10
  distanceFromLeftSide = start
  step = 1
  while True:
      person(distanceFromTop, distanceFromLeftSide, step)
      distanceFromLeftSide -= 1
      step += 1
      if distanceFromLeftSide<=0:
          distanceFromLeftSide = start

      if step > 9:
          step = 1

      time.sleep(0.05)
      os.system('clear')


animate()
