import turtle

window = turtle.Screen()
window.title("Lines 2")
window.setup(width=400, height=400, startx=None, starty=None)
window.mode('logo')

# ========= given code ===============
def drawLine(xFrom, yFrom, xTo, yTo, color, myPen):
   myPen.penup()
   myPen.color(color)
   myPen.goto(xFrom, yFrom)
   myPen.pendown()
   myPen.goto(xTo, yTo)
 
def drawCircle(x, y, radius, color, myPen):
   myPen.setheading(0)
   myPen.penup()
   myPen.color(color)
   myPen.goto(x, y-radius)
   myPen.pendown()
   myPen.circle(radius)
 
def drawSquare(x, y, size, color, myPen):
   myPen.setheading(0)
   myPen.penup()
   myPen.goto(x,y)
   myPen.color(color)
   myPen.fillcolor(color)
   myPen.pendown()
   myPen.begin_fill()
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.left(90)
   myPen.forward(size)
   myPen.end_fill()

myPen = turtle.Turtle()
myPen.hideturtle()
myPen.speed(500)

for x in range (0,2):
  for y in range(0,5):
    if y==0:
      if x==0:
        drawLine(-90, 0, -10, 0, "purple", myPen)
      else:
        drawLine(40, 0, 120, 0, "purple", myPen)
    elif y==1:
      if (x==0):
        drawLine(-100, 10, -90, 0, "purple", myPen)
        drawLine(-100, -10, -90, 0, "purple", myPen)
      else:
        drawLine(50, 10, 40, 0, "purple", myPen)
        drawLine(50, -10, 40, 0, "purple", myPen)
    else:
      if (x==0):
        drawLine(0, 10, -10, 0, "purple", myPen)
        drawLine(0, -10, -10, 0, "purple", myPen)
      else:
        drawLine(110, 10, 120, 0, "purple", myPen)
        drawLine(110, -10, 120, 0, "purple", myPen)

# ========= given code ===============
window.exitonclick()
