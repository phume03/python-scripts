#! /usr/bin/env python
# @author: Phumelela Mdluli
# @name: Rice Krispies
# @description: The name is changed from the famous soggies problem, with General Mills. It is not a rehash, just has the name changed. Simulation of searching for a secret prize in a box of Rice Krispies. How many randomly chosen boxes until I complete my collection? My objective is to COLLECT ALL THE CARDS.
# @license:

import random
def ricekrispies():
    sum = 0
    trials = -1
    trialMin = -1
    trialMax = -1
    max = 0
    min = 1000

    # repeat the experiment 25 times
    for trial in range(1,26):
        collection = []
        
        # Collecting 11 Cards, numbered 1 to 10
        for x in range(11):           
            collection.append(0)
            
        card = random.randrange(11)
        boxes=0
        zeroBoxes=0
        for i in range(11):
            while collection[i]==0:
                card = int(card)
                collection[card] += 1                  
                boxes += 1
                zeroBoxes = collection[0]+1 # collection[0] is always 1
                zero = zeroBoxes-1 # should always be 1
                card = random.randrange(11)

        finalBoxes = boxes - zero # not including the one box which gave us card 0
        trials = trial

        # Update experimental results
        sum = sum + finalBoxes
        if finalBoxes>max:
          max=finalBoxes
          trialMax = trial
 
        if finalBoxes<min:
          min = finalBoxes
          trialMin = trial
         
        print "On trial" ,trial,",the number of boxes required to complete the collection is" ,finalBoxes, "."

    ave = sum/trials
    print "\nAfter",trials,"trials the average number of boxes required to complete the collection was",ave,"boxes."
    print "The trial with the fewest number of boxes was",trialMin, "with a total of",min,"."
    print "The trial with the most number of boxes was",trialMax, "with a total of",max,"."
 
ricekrispies()
