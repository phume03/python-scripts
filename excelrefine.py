#! /usr/bin/env python
import xlrd
import os
#import xlwt

global fileName, excelBook, excelSheet, excelSheetInformation, version, appName
"""
  @author: Phumelela Mdluli
  @description: a simple file to refine excel data. It is advisable that you open up your file and review its contents before you run it through this script.
  @license: GPL v2.0
"""
appName = "Excel Refine"
version = "1.6"
fileName = ""
excelBook = ""
excelSheet = ""
excelSheetInformation = ""

def excelRefine():
  global fileName, excelBook, excelSheet, excelSheetInformation
  intro = introduction() # This section introduces the program.
  if (1 != intro):
    print "Error: failed to startup. Terminating the script."
    return 0
  book = fileread() # This function reads from the excel file. It is faster to read the document once and access the information from memory.
  if (1 != book):
    print "Error: failed to read file. Terminating the script."
    return 0
  print "\nFile or workbook read. Finding a sheet to read."
  sheet = sheetread()
  if (1 != sheet):
    print "Error: failed to read workbook. Terminating the script."
    return 0
  print "\nSheet read. Processing spreadsheet data: searching for heading row."
  heading = sheetheading()
  if (1 != heading):
    print "Error: failed to read heading. Terminating the script."
    return 0
  print "\nHeading processed. Sheet processing continuing..."
  data = sheetdata() # NB: count is the row # of the row with the column headings.
  if (1 != data):
    print "Error: failed to read data. Terminating the script"
    return 0
  return 1
 
# Introduce the program
def introduction():
  introduction = "Excel Data Filter 1.0.1 (c) May 2013\n"
  introduction = introduction+"------------------------------------\n"
  introduction = introduction+""
  introduction = introduction+"Authors: Phumelela Mdluli\n"
  print introduction
  return 1

# Read an excel file
def fileread():
  global excelBook, fileName
  end = False
  while end == False:
    directory = os.getcwd()
    print "The current working directory is: "+directory+". Would you like to change it?"
    response = raw_input("Y/N ")
    response = response.strip()
    if (response == "Y" or response == "y"):
      directory = raw_input("Enter the complete directory path for your file: ")	
      os.chdir(directory)
    else:
      # keep default directory
      pass
    fileName = raw_input("Please enter the file name (do not include the extension): ")
    directory = directory+os.sep+fileName
    end = True
    try:
      tempFilename = directory+".xlsx"
      excelBook = xlrd.open_workbook(tempFilename, "rb") #to handle office 2007 (ext: .xlsx)
    except:
      print "File not in xlsx format."
      try:
        tempFilename = directory+".xls"
        excelBook = xlrd.open_workbook(tempFilename, "rb") #to handle office 2003 or earlier (ext: .xls)
      except:
        try:
          tempFilename = directory+".ods"
          excelBook = xlrd.open_workbook(tempFilename, "rb") #to handle open office docs (ext: .ods)          
        except:
          print "File not in xls format.\nFile could not be found.\nPlease check the file name.\nWould you like to try again?"
          response = raw_input("(Y/n)")
          response = response.strip()
          if (response == "Y" or response == "y"): 
            end = False
          else:
            pass
        finally:
          pass
      finally:
        pass
    finally:
      pass

  return 1

# Read the excel book to expose sheets
def sheetread():
  global excelSheet, excelBook, fileName, excelSheetInformation
  sheetFound = False
  while (sheetFound == False):     
    print "----------------------------------"
    print "(1)  Show sheet name(s). "
    print "(2)  Select sheet: "
    response = raw_input("----------------------------------\n")
    response = int(response)

    if (response == 1):
      print "----------------------------------"
      print "Sheets found in ",fileName, "\n"
      count = 1
      for sheet_name in excelBook.sheet_names():
        print "(",count,")",sheet_name
        count += 1
      print "----------------------------------"
      sheetFound = False
    elif (response == 2):
      sheetRef = raw_input("Enter the sheet by name or numeric-index: ")
      if (sheetRef.isdigit()):
        sheetRef = int(sheetRef)
        sheetRef -= 1
        excelSheet = excelBook.sheet_by_index(sheetRef)
        sheetFound = True
      elif (sheetRef.isalpha()):
        excelSheet = excelBook.sheet_by_name(sheetRef)
        sheetFound = True
      else:
        print "----------------------------------"
        print "Input not recognize, please try again. "
        sheetFound = False        
    else:
      sheetFound = True
      return 0

  excelSheetInformation = {
    'sheetName':excelSheet.name,
    'rowRange':excelSheet.nrows,
    'colRange':excelSheet.ncols,
    'headingsFound':False,
    'heading':{
      'rowNum':0,
      'content':" "
    },
    'count':0
  }

  return 1

# Identify the column headings
def sheetheading():
  global excelSheet, excelSheetInformation
  row = excelSheetInformation['rowRange']
  col = excelSheetInformation['colRange']
  count = excelSheetInformation['heading']['rowNum']
  headingsFound = excelSheetInformation['headingsFound']
  cellContent = []
  output = ""
  while ((headingsFound==False) and (count<row)):
    print "--------------------------------------"
    cellContent = excelSheet.row_values(count,start_colx=0, end_colx=col)
    count = count + 1
    output = ""
    for x in range (len(cellContent)):
      cellContent[x] = str(cellContent[x])
      if (cellContent[x] == " "):
        output += "  "
      else:
	output += cellContent[x]
	output += str(" | ")
		
    print "Headings: ", output
    print "--------------------------------------"
    response = raw_input("Enter (c) if these are the correct headings. Otherwise, enter (n) to inspect the next rows... ")
    if (response.lower()=="n"):
      headingsFound=False
      count+=1
    elif (response.lower()=="c"):
      print "\nHeadings found on row number ",count
      headingsFound=True
    else:
      print "Unrecognized input"
      headingsFound = False
      count=row
    
    print row, col, count, headingsFound
    pass
  excelSheetInformation['headingsFound'] = headingsFound
  excelSheetInformation['heading']['rowNum'] = count
  excelSheetInformation['heading']['content'] = output
  return 1

def sheetdata():
  global excelSheet, excelSheetInformation
  excelSheetInformation = {
    'sheetName':excelSheet.name,
    'rowRange':excelSheet.nrows,
    'colRange':excelSheet.ncols,
    'headingsFound':False,
    'heading':{
      'rownum':0,
      'content':" "
    }
  }

  row = excelSheetInformation['rowRange']
  col = excelSheetInformation['colRange']
  count = excelSheetInformation['count'] 
  headings = excelSheetInformation['headingsFound']
  cellContent = excelSheetInformation['cellContent']
  output = excelSheetInformation['output']
  
  #Identify the row with the table headings
  print "Which row contains the column headings? "  
  while ( headings == False and count < row ):
    print "----------------------------------------------------------------------------------------------"
    cellContent = excelSheet.row_values(count,start_colx=0, end_colx=col)
    for x in range (len(cellContent)):        
      cellContent[x] = str(cellContent[x])
      if (cellContent[x] == " "):
        output += "   "
      else:
        output += cellContent[x]
        output += str("  |  ")   
        
    print "Headings: ", output
    print "----------------------------------------------------------------------------------------------"    
    response = raw_input("Enter (c) if these are the correct headings. Otherwise, enter (n) to inspect the next rows... " )
    if ( response == "n"):
      headingsFound = False
      count += 1
    else:
      if (response == "c"):
        headingsFound = True
      else:
        print "Unrecognized input"
        headingsFound = False
  
  excelSheetInformation['rowRange'] = row
  excelSheetInformation['colRange'] = col
  excelSheetInformation['count'] = count
  excelSheetInformation['headingsFound'] = headings
  excelSheetInformation['cellContent'] = cellContent
  excelSheetInformation['output'] = output
  return 1

run = excelrefine()
if (1 != run):
  print "Error: failed to run. Terminating the script."

## ================================================================
## ================================================================
## ================================================================

  """
  readNextSheet=True
  while(readNextSheet == True):
    sheet = sheetread() # Given any document, we first have to know which sheet to read from.
    if (1 != sheet):
      print "Error: failed to read sheet(s). Terminating the script."
      return 0

    print "\nRead ",excelSheetInformation['sheetName'],". Processing spreadsheet data."    
    data = sheetdata()
    if (1 != data):
      print "Error: failed to read data. Terminating the script."
      return 0

    print "Headings found. Preparing to edit the data."
    DoNothing=False
    while (DoNothing == False):
      print "\nHow would you like to process each column?"
      print excelSheetInformation['heading']['content']
      print "\n0) Do nothing! \n1) Change Text to Upper Case \n2) Change Text to Lower Case \n3) Change Text to Title Format (proper noun) \n4) Change Text Sentence Format (first word to proper noun)\n5) Check that the contents are words only \n6) Check that the contents are numbers only \n7) Check that the contents are alphanumeric \n8) Process column statistics"   
      response = raw_input("")
      response = int(response)
      if (response != 0):
        column = raw_input("Please enter the column reference of the COLUMN to edit?")
        column = int(column)
        if (response == 1):
          changeToUpperCase(column)
        elif (response == 2):  
          changeToLowerCase(column)
        elif (response == 3):
          changeToProperNounCase(column)
        elif (response == 4): 
          changeToSentenceCase(column)
        elif (response == 5):
          processWords(column)
        elif (response == 6):
          processNumbers(column)  
        elif (response == 7):
          processAlphaNumeric(column)
        elif (response == 8):
          processColumnStats(column)
        else:
          print "Error: unexpected input!"
          DoNothing=True

        print "Column",column,"edited."
      else:
        print "No spreadsheet operation selected."
        DoNothing=True

    print "Done editing ",excelSheetInformation['sheetName'],". Saving the changes to workbook."
    complete = savesheet()
    if (1 != complete):
      print "Error: failed to save sheet data. Terminating the script."
      return 0

    response = raw_input("Changes written to workbook. Would you like to edit another sheet? Y/N")
    response = response.strip()
    if (response== "Y" or response=="y"):
      readNextSheet = True
    elif (response=="N" or response=="n"):
      readNextSheet = False
    else:
      print "Error: unexpected response. Proceeding to saving the workbook."

  print "Done editing the workbook. Saving the changes to file."
  complete = savefile()
  if (1 != complete):
    print "Error: failed to write data."
    return 0

  print "Changes written to file. Exiting the script."
  return 1
  """

  """	
	#Given any document, we first have to know which sheet to read from.
	print "\nFile read. Select the next option: "

	
	#
	processing=True
	while (processing):
		print "How would you like to proceed?\n(1) Print entire sheet\n(2) Print row by row\n(3) Edit the current sheet\n(4) End Program"
		response = raw_input(">> ")
		if (response=="1"):
			print "Heading: ",output # consider variable scope here!
			printFromSheet(sheet,True,count)
		elif (response=="2"):
			print "Heading: ",output # consider variable scope here!
			printFromSheet(sheet,False,count)
		elif (response=="3"):
			editSheet(sheet)
		elif (response=="4"):	
			print "User terminated program."
			processing=False
		else:
			print "Error. Unrecognized input. Terminating program."
			processing=False
  """
# ============================================================================================>>>

def editSheet(mySheet):
	print """\nWhat edits would you like to implement?
	
	1) Convert cell text to Uppercase
	2) Convert cell text to Lowercase
	3) Convert first letter of first word to Uppercase

	"""
	editOption = raw_input(">>")

	print  """\nWhich column would you like to apply this to?"""
	
	if (editOption==1):
		
	elif (editOption)==2:
	elif (editOption==3):

def printFromSheet(mySheet, _ALL, HeadingsRowNum):
	# get sheet variables
	rowRange = mySheet.nrows-1
	colRange = mySheet.ncols
	count = HeadingsRowNum+1
	terminate=False	

	if (_ALL):
		print "--------------------------------------"

	while (terminate==False and count<=rowRange):
		# PRINT BY ROW
		if (_ALL==False):
			print "--------------------------------------"
			cellContent = mySheet.row_values(count,start_colx=0, end_colx=colRange)
			output = ""
			for x in range (len(cellContent)):
				cellContent[x] = str(cellContent[x])
				if (cellContent[x] == " "):
					output += "  "
				else:
					output += cellContent[x]
					output += str(" | ")
		
			print "Row ",count+1,": ", output
			print "--------------------------------------"
			response = raw_input("Enter (n) to inspect the next rows, or enter (c) to cancel row inspection... ")
			if (response.lower()=="n"):
				terminate=False
				count+=1
			else:
				if (response.lower()=="c"):
					terminate=True
				else:
					print "Unrecognized input"
					terminate=True
					continue
		# PRINT ALL
		else:
			terminate=False	
			cellContent = mySheet.row_values(count,start_colx=0, end_colx=colRange)
			output = ""
			for x in range (len(cellContent)):
				cellContent[x] = str(cellContent[x])
				if (cellContent[x] == " "):
					output += "  "
				else:
					output += cellContent[x]
					output += str(" | ")
			print output
			print count, rowRange
			count+=1


# -----Reserve-Code--------------------------------------------------------->>
"""
  

def savesheet():
  global excelBook, excelSheet, excelSheetInformation
  
  styleNumber = xlwt.easyxf('font: name Times New Roman, color-index red, bold on', num_format_str='#,##0.00')
  styleDate = xlwt.easyxf(num_format_str='D-MMM-YY')
  styleText = xlwt.easyxf('font: name Times New Roman, color-index black, bold off')
  excelSheetInformation = {'sheetName':excelSheet.name,'rowRange':excelSheet.nrows,'colRange':excelSheet.ncols,'heading':{'rownum':0,'content':" "}}

  return 1


def savefile():
  global fileName, excelBook, excelSheet, excelSheetInformation
  """
  # ---- Reserve-Code ---->>

  --Module code-----
  xlrd.open_workbook        Returns a workbook to edit.

  --Workbook code---
  book.nsheets              Returns the number of sheets in a workbook.
  book.sheet_names()        Returns the names of the sheets in the workbook.
  book.sheet_by_index(0)    Returns a sheet onject.

  --Sheet code-----
  sheet.name                          Returns the name of the sheet.
  sheet.nrows                         Returns the number of rows.
  sheet.ncols                         Returns the number of columns.
  sheet.cell(row_index, col_index)    Returns the cell object referenced.
  sheet.cell(row_index, col_index)    Returns the cell object referenced
  sheet.row_values()                  Returns a list of cells

  --Cell code------
  cell.value                          Returns the contents of the referenced cell. 
  cell.colname(col_index)
"""

"""  

  global fileName, excelBook
  _fileName = fileName.split(".")
  fileName = _fileName[0]+"_save"+"."+_fileName[1]
  excelBook.save(fileName)
  return 1


def changeToUpperCase(col_index):
  global excelSheet, excelSheetInformation
  row = excelSheetInformation['rowRange'] # not to be overwritten

  for data_row in range (1, row):
    value = excelSheet.row_values(data_row,start_colx=col_index,end_colx=col_index+1)
    print value
    # .upper()
  return 1


def changeToLowerCase(col_index):
  global excelSheet, excelSheetInformation
  row = excelSheetInformation['rowRange'] # not to be overwritten

  for data_row in range (1, row):
    value = excelSheet.row_values(data_row,start_colx=col_index,end_colx=col_index+1)
    print value
    # .lower()

  return 1


def changeToProperNounCase(col_index):
  global excelSheet, excelSheetInformation
  row = excelSheetInformation['rowRange'] # not to be overwritten

  for data_row in range (1, row):
    value = excelSheet.row_values(data_row,start_colx=col_index,end_colx=col_index+1)
    print value
    # .title()

  return 1


def changeToSentenceCase(col_index):
  global excelSheet, excelSheetInformation
  row = excelSheetInformation['rowRange'] # not to be overwritten

  for data_row in range (1, row):
    value = excelSheet.row_values(data_row,start_colx=col_index,end_colx=col_index+1)
    print value
    # .capitalize()

  return 1


def processWords(col_index):
  global excelSheet, excelSheetInformation
  row = excelSheetInformation['rowRange'] # not to be overwritten

  for data_row in range (1, row):
    value = excelSheet.row_values(data_row,start_colx=col_index,end_colx=col_index+1)
    print value

  return 1


def processNumbers(col_index):
  global excelSheet, excelSheetInformation
  row = excelSheetInformation['rowRange'] # not to be overwritten

  for data_row in range (1, row):
    value = excelSheet.row_values(data_row,start_colx=col_index,end_colx=col_index+1)
    print value

  return 1


def processAlphaNumeric(col):
  global excelSheet, excelSheetInformation
  row = excelSheetInformation['rowRange'] # not to be overwritten

  for data_row in range (1, row):
    value = excelSheet.row_values(data_row,start_colx=col_index,end_colx=col_index+1)
    print value

  return 1


def processColumnStats(col):
  global excelSheet, excelSheetInformation
  row = excelSheetInformation['rowRange'] # not to be overwritten

  for data_row in range (1, row):
    value = excelSheet.row_values(data_row,start_colx=col_index,end_colx=col_index+1)
    print value

  return 1


def min():

  return 1

 
def max():

  return 1


def mode():

  return 1


def average():

  return 1


def sum():

  return 1

