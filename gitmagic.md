# GIT MAGIC
Every once in a while something happens with Git that is beyond create a branch, add files to branch, commit to branch, merge two branches, and push local to remote. It is very rare because by convention, GIT should be used in a rather linear fashion. Infact, one person on the web suggested that git was not made for things to be removed. That is, the stupid content manager, was not designed with stupidity in mind. Ever so often, mistakes do happen and when they happen, we have to "firefight" to get things going again.

I just learned about git filter branch, which is more powerful than the git rm command, whose use I still doubt when we have the rm or del commands on the CLI. In anycase, inspired by this, I have decided to write down this command for you all and myself to remember later on:

sudo git filter-branch --force --index-filter   "git rm --cached --ignore-unmatch ./sheet" --prune-empty --tag-name-filter cat -- --all

This is a very complicated script. But it is one of the most powerful or effective scripts out there. Use it wisely.
