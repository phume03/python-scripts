#! /usr/bin/env python
# @author: Phumelela Mdluli
# @description: A simple algorithm to simulate the roll of a six (6) sided die, 600 times.

import random
def dice():
    freq = []
    # Array inherently begins at 0 and range function terminates one number before the stated index. We need to track the occurance of numbers 1-6.
    for i in range(0,7):
        freq.append(0)

    # To get 600 simulations, we have to go from 0-599. Plus we do not want to produce the number 0 in the simulation
    for roll in range(600):
        result = random.randrange(1,7)
        freq[result]=freq[result]+1
 
    # Print out our results with a check that the data we have is accurate
    count=0
    for i in range(1,7):
        print "The number of",i,"'s is " ,freq[i],"."
        count = count+freq[i]

    print "The die was rolled ",count," times."

# Edited for terminal use
dice()
