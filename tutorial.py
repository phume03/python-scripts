#! /usr/bin/env python
# @author: Phumelela Mdluli
# @name: Python Tutorial
# @description: these were meant to be my personal notes from taking a python tutorial, however in an effort to participate in the developer community a tutorial was born. I have added the work of Steve Howell, and Eric Araujo from wiki.python.org (Website: PythonInfo Wiki, Page Title: Simple Programs, Last Edited: 2012-05-11, Last Accessed: 2012-06-16). I found their work to be helpful and thought to bring all of it into a centralized place with my work. However, I will maintain the original links if you wish to visit those sites at your own leisure. Some problems come from inventwithpython.com (courtesy mention), but I did not personally use it.
# @improvements: introduce a GUI to make it easier for absolute beginners.
# @notice: all code in this file is open source, with the standard Python license. So, please feel free to contribute. Minor cleanups are welcome, but if you want to do major restructuring, please run them by me or simply make your own copy of this file. This note was copied from the original page by Steve Howell, so most of it are his sentiments and bylaws. However, I "too" hope that this code is useful for you!


def quitFunc():
	print "Done"
	spaceFunc()


def basicsFunc():
	print "***Basics***"
	print ""
	x=3
	f=3.141529
	name="Python"
	big=358315791L
	z=complex(2,3)
	print "The python programming language makes use of variables. A variable is specially assigned, temporary storage space in random access memory. Variables are used to store data while a program executes. Variables can store;\nintegers e.g. x= integer\nSo, x=",x,",\nfloating points e.g. f=float\nSo,f=",f,",\nstrings e.g. name='string'\nSo, name=",name,",\nlong integers e.g. big=000L\nSo,big=",big,",\ncomplex numbers e.g. z=complex(2,3)\nSo, z=",z,"."
	print ""
	print "We can perform operations (arithmetic, string manipulation, or general data manipulation) using the stored data. For example;"
	x=3
	y=8
	print "x=",x
	print "y=",y
	print "sum = x+y"
	sum=x+y
	print "So, sum=",sum
	spaceFunc()


def ctrlFlow():
	print "***Control Flow***"
	print "This is an exercise in simple flow logic. We can make the program decide which code fragment to execute at any one given moment during execution. Consider the following pseudo code;"
	print "if x is comparable to (some-value)\n\texecute this code\nelse\n\texecute this code"
	print ""
	print "In python syntax, this reads;"
	print "if x < y:\n\tprint x is less than y\nelse:\n\tprint x is greater than or equal to y"
	print ""
	print "Note two things, mathematics rules of inclusivity and exclusivity, and the comparison in the \'else\' statement is implied. Let us see our code in action:"
	print ""
	print "Let y be 10;"
	x=3
	print "x = ",x
	if x<10:
		print "x is smaller than 10"
	else:
		print "x is bigger than 10 or equal"
	print ""
	age=30
	print "More applications.\nage = ",age
	print "- Guess my age, you have 1 chance!"
	guess = int(raw_input("Your Guess: "))
	if guess!=age:
		print "Wrong!"
	else:
		print "Correct"

	print "We can nest conditional statements forming more complicated logic statements."
	a=12
	b=33
	print "a = ",a
	print "b = ",b
	if a>10:
		if b>20:
			print "Good"
	print "The previous conditions, test if a is greater than 10, and then, if b is greater than 20. What happens if anyone is false? What if a is greater than 10 but b is less than 20? What if b is greater than 20 and a is less than 10?"
	print "Let us play the guessing game again."
	print "- Guess my age!"
	guess = int(raw_input("Your Guess: "))
	if guess>10 and guess<20:
		print "In range"
	else:
		print "Out of range"
	print "According to the output, we have both an upper-bound and a lower bound on our test. What are the bounds, are you certain of them? How did the programmer manage this?"
	print "Finally, note that our guessing games only run once. What if I want to give the user more than one guess? What if the user would like to play multiple times without restarting the whole application?\nFor this we would use \'LOOPS\'. There are different kinds of loops, and each one applies in very specific situations."
	spaceFunc()


def userInput():
	print "***User Input***"
	print ""
	print "The python programming language recognizes user input from the keyboard. Recognition of keystrokes allows the programmer to create interactive programs. They are called interactive because the user input interupts the flow of the code. Consider programs such as the games on your mobile telephone device (cellphone). These games are typically designed to accept information from the cellphone user, e.g. a name for the user with the highest score."
	print ""
	print "Try it yourself..."
	x=int(raw_input("Enter an integer value for x: "))
	y=int(raw_input("Enter an integer value for y: "))
	sum=x+y
	print "Remember sum=x+y,"
	print "So, sum=",sum
	print ""
	print "The function to use is raw_input() e.g. raw_input('message to user'). We can store the entered result into a variable for later use i.e. name=raw_input('enter your name'). The python programming language always accepts user data as a string. Before we can perform arithmetic operations, we need to tell python to convert the data into an integer. To do this, we use the int() function. This function accpets a string as a parameter, and outputs an integer. "
	spaceFunc()


def stringsFunc():
	print "***Common String Operations***"
	print ""
	print "If you started with the basics, you may have learned that python can store string data to memory (variables). This is a lesson in string basics."
	s="Hello Python"
	print "s: ",s
	print "s[0]: ",s[0]
	print "s[0:2]: ",s[0:2]
	print "s[3:5]: ",s[3:5]
	print "s[6:]: ",s[6:]
	print "s+' '+s: ",s+' '+s
	print "We can edit a string using the replace function. It will change all occurrences of a particular word with another. So, s.replace('Hello','Thanks'): ",s.replace('Hello','Thanks')
	print ""
	print "Python allows you to perform comparisons between two string variables."
	sentence1="The cat is blue"
	sentence2="cat"
	print "sentence 1: ",sentence1
	print "sentence 2: ",sentence2
	print "if sentence 1 equals sentence 2\n\tprint equal\nelse if sentence 1 is not equal to sentence 2\n\tprint not equal"
	print ""
	if sentence1==sentence2:
		print 'equal'
	else:
		print 'not equal'
	print ""

	print "We may also use the \"in\" keyword to check if one string contains another."
	print "if sentence 2 is in sentence 1\n\tprint sentence 2 was found in sentence 1\nelse if sentence 2 is not in sentence 1\n\tprint nothing"
	if sentence2 in sentence1:
		print sentence2+" was found in "+sentence1
	print ""
	s="Hello world"
	print "s: ",s
	print "length of s: ",len(s)
	s=s.upper()
	print "convert s to uppercase (s.upper()): ",s
	print "To convert s to lowercase, use s.lower()."
	str1="Python"
	str2=" is an okay beginner programming language."	
	print "String 1: ",str1
	print "String 2: ",str2
	print "String 1 + String 2: ",str1+str2	#print a concatenated string
	print "The python programming language uses the arithmetic plus (+) symbol to join or concatenate strings together."
	print ""
	print "In programming, a string of length 1 is normally referred to as a character. Computer languages were designed to represent most of the characters that we use in written languages. Some characters cannot be written directly into our code from the keyboard, as we program, because of the way a computer operates. For example, the space and tab characters. Thus, the programmer employs special symbolism to represent these characters in strings. \nFor example:\n- newline character (\\n), \n- tab character (\\t)"
	print ""
	print "Sometimes the programmer may wish to represent quoted (borrowed text) in a string. Quotation marks are coded as special characters (\\\"). \nThe word \"computer\" is in quotes.\n This convention prevails in most programming languages, as such this tutorial does not include an exhaustive list of the special characters."
	spaceFunc()


def listsFunc():
	print "***Lists***"
	print "A list is a collection of items. A list can be ordered or unorderedd. Python provides several options for creating lists; a typed array/list, a set/tuple, dictionary and the optional class.\nThe python array is special because arrays are typically immutable. However, with the python list, we can not only edit its contents, but we can add or remove items thus affecting the array size (or number of indexes). The set/tuple is an unkeyed list of items, similar to a traditional array. The dictionary is similar to the javascript object notation or JSON object. We can say a dictionary is a key-value pair collection of items. Each entry into the dictionary has a corresponding key with which to reference the entry. The class provides a very special way of creating collections, and is regarded as an advanced data structure. A class is special because it is user-defined, and it can contain any other data type that is possible in python, including other user-defined data types. More importantly, a class can also contain user-defined methods for manipulating the data and datastructures within the class. Thus, it is considered special, and careful consideration has to be put into writing classes. Provided below are examples of each data structure as well as clearer definitions of what they are.\nBelow are some examples of the list data structure and some common ways of using it:\n"
	l=["Blue","Red","Yellow","Black"]
	print "list = ",l		#prints all elements in the list/array
	print "First list element (list[0]): ",l[0]	#prints the element at index 0
	print "Second list element (list[1]): ",l[1]	#prints the second elements in the list (index 1)
	l.append("Green")
	print "Add \"green\" to the list (list.append(green))."
	print "list = ",l
	print "Remove \"red\", and \"yellow\" from the list (list.remove(color))."
	l.remove("Red")
	l.remove("Yellow")
	print "list = ",l
	l.sort()
	print "Sort the list (list.sort())."
	print "list = ",l
	l.reverse()
	print "Reverse the list (list.reverse())."
	print "list= ",l
	print ""
	print "Python Tuples: a sequence of data elements separated by a comma e.g point = (m,n), point[0] = m, and point[1] = n. Below are some examples of the tuple data structure and some of the common ways that it is used:"
	point=(3,4)
	point2=(2,6,12)
	print "(x,y) = ",point
	print "x = ",point[0]
	print "y = ",point[1]
	print "(a,b,c) = ",point2
	print "a = ",point2[0]
	print "b = ",point2[1]
        print "c = ",point2[2]
	print ""
	print "Python Dictionaries: an unordered set of key, value pairs. A dictionary object is enclosed by a pair of curly braces. e.g. dict = {key 1: value 1, key 2: value 2}. Below are some examples of the dictionary data structure and some of the common ways that it is used:"
	words={}
	words["Hello"]="Bonjour"
	words["Yes"]="Oui"
	words["No"]="Non"
	words["Bye"]="Au Revoir"
	print "words = ",words
	print "words[\"Hello\"] = ",words["Hello"]
	print "words[\"No\"] = ",words["No"]
	print "Note that array, and tuple data is referenced using the data's place in the data-structure. On the other hand, dictionary data is referenced using the key."
	dictionary={}
	dictionary["Ford"]="Car"
	dictionary["Python"]="The Python Programming Language"
	dictionary[2]="This sentence is stored here."
	print "dictionary = ",dictionary
	print "dictionary[\'Ford\'] = ",dictionary['Ford']
	print "dictionary[\'Python\'] = ",dictionary['Python']
	print "dictionary[2] = ",dictionary[2]
	print "words = ",words
	del words["Yes"]
	print "To remove a word from the dictionary, use \"del words[key]\"."
	print "words = ",words
	print "We can reuse the old key, \"Yes\"."
	words["Yes"]="Oui!"
	print "words = ",words
        spaceFunc()       


def typecasting():
	print "***DataType Casting***"
	x=3
	y=2.153153153153153153	
	print "If we assign two variables x and y integer values, we can change the data-type from integer to string using the str(integer) function. This is called type casting. Specifically, this is called explicit type casting. Some languages perform implicit type casting on compilation. \nGiven your understanding of string and integer operations, why do you think that this is a significant operation? \nHow can we reverse this operation?"
	print "x = ",x
	print "str(x) = \""+str(x)+"\""
	print "y = ",y
	print "str(y) = \""+str(y)+"\""
	print ""
	a="135.31421"
	b="133.1112223"
	print "a = \""+a+"\""
	print "b = \""+b+"\""
	c=float(a)+float(b)
	print "c = float(a)+float(b) = ",c
	spaceFunc()


def spaceFunc():
	print "\n--------------------------------------------\n"


def functions():
	print "***Functions***"
	print "A function is a group of code performing a particular operation. A function can have zero or more inputs, and zero or more outputs. By coding convention, it is prefered that a function have one output so that other programmers can trace what parts of the code the function affects. In some languages, a function may be referred to as a method among other names."
	print ""
	print "To declare a function in python, one must use the \'def\' keyword, followed by the function name. For example;\n\t\'def\' function_name(input 1, input 2, input x):\n\t\tinsert function code here\n\t\treturn output"
	print ""
	print "From the function, the returned output can be a variable, a print statement, or an indicator of function success or failure e.g. 0, 1, or -1. The output of the function can be used in another calculation, or stored in a variable if the function is called after a variable. If no return is declared, the function exits after execution." 
	spaceFunc()


# @description: I skipped the basics as a tutorial precedes these examples. But python can be fun to play with!
def funExamples():
  repeat=1
  print "***Fun Examples***"
  name = raw_input('What is your name?\n')
  print 'Hi, %s ! These are interactive python examples.' % name
  # print 'Hi, ",name,"! These are interactive python examples.'
  
  while(repeat==1):
    response=raw_input("\nWhich example would you like to see?\n0)None\n1)For-loop, built-in enumerate function, new style formatting\n2)Fibonacci, tuple assignment\n3)Import, regular expressions\n4)Dictionaries, generator expressions\n5)Command line arguments, exception handling\n6)Opening files\n7)Time, conditionals, from..import, for..else\n8)Triple-quoted strings, while-loop\n9)Classes\n10)Unit testing with unittest\n11)Doctest-based testing\n12)Itertools\n13)CSV module, tuple upacking, cmp() built-in\n14)8-Queens Problem (recursion)\n15)Prime numbers sieve with fancy generators\n16)XML/HTML parsing\n17) 8-Queens Problem (define your own exceptions)\n18)Guess the number\n")
    response=int(response)
    print ""
    
    if (response==1):
      code='''friends = ['john', 'pat', 'gary', 'michael']\nfor i, name in enumerate(friends):\n\tprint "iteration {iteration} is {name}".format(iteration=i, name=name)'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      friends = ['john', 'pat', 'gary', 'michael']
      for i, name in enumerate(friends):
        print "iteration {iteration} is {name}".format(iteration=i, name=name)


    elif (response==2):
      code='''parents, babies = (1, 1)\nwhile babies < 100:\n\tprint 'This generation has {0} babies'.format(babies)\n\tparents, babies = (babies, parents + babies)'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      parents, babies = (1, 1)
      while babies < 100:
        print 'This generation has {0} babies'.format(babies)
        parents, babies = (babies, parents + babies)


    elif (response==3):
      code='''import re\nfor test_string in ['555-1212', 'ILL-EGAL']:\n\tif re.match(r'^\d{3}-\d{4}$', test_string):\n\t\tprint test_string, 'is a valid US local phone number'\n\telse:\n\t\tprint test_string, 'rejected'
      '''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      import re
      for test_string in ['555-1212', 'ILL-EGAL']:
        if re.match(r'^\d{3}-\d{4}$', test_string):
          print test_string, 'is a valid US local phone number'
        else:
          print test_string, 'rejected'


    elif (response==4):
      code='''prices = {'apple': 0.40, 'banana': 0.50}\nmy_purchase = {'apple': 1,'banana': 6}\ngrocery_bill = sum(prices[fruit] * my_purchase[fruit] for fruit in my_purchase)\nprint 'I owe the grocer $%.2f' % grocery_bill'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      prices = {'apple': 0.40, 'banana': 0.50}
      my_purchase = {'apple': 1, 'banana': 6}
      grocery_bill = sum(prices[fruit] * my_purchase[fruit] for fruit in my_purchase)
      print 'I owe the grocer $%.2f' % grocery_bill


    elif (response==5):
      code='''#!/usr/bin/env python\n# This program adds up integers in the command line\nimport sys\ntry:\n\ttotal = sum(int(arg) for arg in sys.argv[1:])\n\tprint 'sum =', total\nexcept ValueError:\n\tprint 'Please supply integer arguments'
      '''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      import sys
      try:
        total = sum(int(arg) for arg in sys.argv[1:])
        print 'sum =', total
      except ValueError:
        print 'Please supply integer arguments'


    elif (response==6):
      code='''# indent your Python code to put into an email\nimport glob\n# glob supports Unix style pathname extensions\npython_files = glob.glob('*.py')\nfor file_name in sorted(python_files):\n\tprint '    ------' + file_name\n\twith open(file_name) as f:\n\t\tfor line in f:\n\tprint '    ' + line.rstrip()\n\nprint'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      import glob
      python_files = glob.glob('*.py')
      for file_name in sorted(python_files):
        print '    ------' + file_name
        with open(file_name) as f:
          for line in f:
            print '    ' + line.rstrip()
        print


    elif (response==7):
      code='''from time import localtime\nactivities = {8: 'Sleeping',9: 'Commuting',17: 'Working',18: 'Commuting',20: 'Eating',22: 'Resting' }\ntime_now = localtime()\nhour = time_now.tm_hour\n\nfor activity_time in sorted(activities.keys()):\n\tif hour < activity_time:\n\t\tprint activities[activity_time]\n\t\tbreak\n\telse:\n\t\tprint 'Unknown, AFK or sleeping!'
      '''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      from time import localtime
      activities = {8: 'Sleeping',9: 'Commuting',17: 'Working',18: 'Commuting',20: 'Eating',22: 'Resting' }
      time_now = localtime()
      hour = time_now.tm_hour
      for activity_time in sorted(activities.keys()):
        if hour < activity_time:
          print activities[activity_time]
          break
        else:
          print 'Unknown, AFK or sleeping!'


    elif (response==8):
      code='''REFRAIN = "%d bottles of beer on the wall,\n%d bottles of beer,\ntake one down, pass it around,\n%d bottles of beer on the wall!"\nbottles_of_beer = 99\nwhile bottles_of_beer > 1:\n\tprint REFRAIN % (bottles_of_beer, bottles_of_beer,bottles_of_beer - 1)\n\tbottles_of_beer -= 1'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      REFRAIN = '''%d bottles of beer on the wall,\n%d bottles of beer,\ntake one down, pass it around,\n%d bottles of beer on the wall!'''
      bottles_of_beer = 99
      while bottles_of_beer > 1:
        print REFRAIN % (bottles_of_beer, bottles_of_beer,bottles_of_beer - 1)
        bottles_of_beer -= 1


    elif (response==9):
      code='''class BankAccount(object):\ndef __init__(self, initial_balance=0):\n\tself.balance = initial_balance\ndef deposit(self, amount):\n\tself.balance += amount\ndef withdraw(self, amount):\n\tself.balance -= amount\ndef overdrawn(self):\n\treturn self.balance < 0\nmy_account = BankAccount(15)\nmy_account.withdraw(5)\nprint my_account.balance'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      class BankAccount(object):
        def __init__(self, initial_balance=0):
          self.balance = initial_balance
        def deposit(self, amount):
          self.balance += amount
        def withdraw(self, amount):
          self.balance -= amount
        def overdrawn(self):
          return self.balance < 0
      my_account = BankAccount(15)
      my_account.withdraw(5)
      print my_account.balance


    elif (response==10):
      code='''import unittest\ndef median(pool):\n\tcopy = sorted(pool)\n\tsize = len(copy)\n\tif size % 2 == 1:\n\t\treturn copy[(size - 1) / 2]\n\telse:\n\t\treturn (copy[size/2 - 1] + copy[size/2]) / 2\nclass TestMedian(unittest.TestCase):\n\tdef testMedian(self):\n\t\tself.failUnlessEqual(median([2, 9, 9, 7, 9, 2, 4, 5, 8]), 7)\nif __name__ == '__main__':\n\tunittest.main()'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      import unittest
      def median(pool):
        copy = sorted(pool)
        size = len(copy)
        if size % 2 == 1:
          return copy[(size - 1) / 2]
        else:
          return (copy[size/2 - 1] + copy[size/2]) / 2

      class TestMedian(unittest.TestCase):
        def testMedian(self):
          self.failUnlessEqual(median([2, 9, 9, 7, 9, 2, 4, 5, 8]), 7)

      if __name__ == '__main__':
        unittest.main()


    elif (response==11):
      code='''def median(pool):\n\t#Statistical median to demonstrate doctest.\n\t#>>> median([2, 9, 9, 7, 9, 2, 4, 5, 8])\n\t#7\n\tcopy = sorted(pool)\n\tsize = len(copy)\n\tif size % 2 == 1:\n\t\treturn copy[(size - 1) / 2]\n\telse:\n\t\treturn (copy[size/2 - 1] + copy[size/2]) / 2\n\tif __name__ == '__main__':\n\t\timport doctest\n\t\tdoctest.testmod()'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      def median(pool):
        copy = sorted(pool)
        size = len(copy)
        if size % 2 == 1:
          return copy[(size - 1) / 2]
        else:
          return (copy[size/2 - 1] + copy[size/2]) / 2

      if __name__ == '__main__':
        import doctest
        doctest.testmod()


    elif (response==12):
      code='''from itertools import groupby\nlines = "\nThis is the first paragraph.\nThis is the second.".splitlines()\n# Use itertools.groupby and bool to return groups of\n# consecutive lines that either have content or don't.\nfor has_chars, frags in groupby(lines, bool):\n\tif has_chars:\n\t\tprint ' '.join(frags)\n# PRINTS:\n# This is the first paragraph.\n# This is the second.'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      from itertools import groupby
      lines = '''
      This is the
      first paragraph.

      This is the second.
      '''.splitlines()
      for has_chars, frags in groupby(lines, bool):
        if has_chars:
          print ' '.join(frags)


    elif (response==13):
      code='''import csv\n# write stocks data as comma-separated values\nwriter = csv.writer(open('stocks.csv', 'wb', buffering=0))\nwriter.writerows([('GOOG', 'Google, Inc.', 505.24, 0.47, 0.09),('YHOO', 'Yahoo! Inc.', 27.38, 0.33, 1.22),('CNET', 'CNET Networks, Inc.', 8.62, -0.13, -1.49)])\n# read stocks data, print status messages\nstocks = csv.reader(open('stocks.csv', 'rb'))\nstatus_labels = {-1: 'down', 0: 'unchanged', 1: 'up'}\nfor ticker, name, price, change, pct in stocks:\n\tstatus = status_labels[cmp(float(change), 0.0)]\n\tprint '%s is %s (%s%%)' % (name, status, pct)'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      import csv
      writer = csv.writer(open('stocks.csv', 'wb', buffering=0))
      writer.writerows([('GOOG', 'Google, Inc.', 505.24, 0.47, 0.09),('YHOO', 'Yahoo! Inc.', 27.38, 0.33, 1.22),('CNET', 'CNET Networks, Inc.', 8.62, -0.13, -1.49)])
      stocks = csv.reader(open('stocks.csv', 'rb'))
      status_labels = {-1: 'down', 0: 'unchanged', 1: 'up'}
      for ticker, name, price, change, pct in stocks:
        status = status_labels[cmp(float(change), 0.0)]
        print '%s is %s (%s%%)' % (name, status, pct)


    elif (response==14):
      code='''BOARD_SIZE = 8\ndef under_attack(col, queens):\n\tleft = right = col\n\tfor r, c in reversed(queens):\n\t\tleft, right = left - 1, right + 1\n\t\tif c in (left, col, right):\n\t\t\treturn True\n\treturn False\ndef solve(n):\n\tif n == 0:\n\t\treturn [[]]\n\tsmaller_solutions = solve(n - 1)\n\treturn [solution+[(n,i+1)]\n\t\tfor i in xrange(BOARD_SIZE)\n\t\t\tfor solution in smaller_solutions\n\t\t\t\tif not under_attack(i+1, solution)]\nfor answer in solve(BOARD_SIZE):\n\tprint answer'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      BOARD_SIZE = 8

      def under_attack(col, queens):
        left = right = col

        for r, c in reversed(queens):
          left, right = left - 1, right + 1

          if c in (left, col, right):
            return True

        return False

      def solve(n):
        if n == 0:
          return [[]]

        smaller_solutions = solve(n - 1)

        return [solution+[(n,i+1)]
          for i in xrange(BOARD_SIZE)
            for solution in smaller_solutions
              if not under_attack(i+1, solution)]
      for answer in solve(BOARD_SIZE):
        print answer


    elif (response==15):
      code='''import itertools\ndef iter_primes():\n\t# an iterator of all numbers between 2 and +infinity\n\tnumbers = itertools.count(2)\n\t# generate primes forever\n\twhile True:\n\t\t# get the first number from the iterator (always a prime)\n\t\tprime = numbers.next()\n\t\tyield prime\n\t\t# this code iteratively builds up a chain of\n\t\t# filters...slightly tricky, but ponder it a bit\n\t\tnumbers = itertools.ifilter(prime.__rmod__, numbers)\nfor p in iter_primes():\n\tif p > 1000:\n\t\tbreak\n\tprint p'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      import itertools
      def iter_primes():
        numbers = itertools.count(2)
        while True:
          prime = numbers.next()
          yield prime
          numbers = itertools.ifilter(prime.__rmod__, numbers)

      for p in iter_primes():
        if p > 1000:
          break
        print p


    elif (response==16):
      code='''dinner_recipe = "<html><body><table>\n\t<tr><th>amt</th><th>unit</th><th>item</th></tr>\n\t<tr><td>24</td><td>slices</td><td>baguette</td></tr>\n\t<tr><td>2+</td><td>tbsp</td><td>olive oil</td></tr>\n\t<tr><td>1</td><td>cup</td><td>tomatoes</td></tr>\n\t<tr><td>1</td><td>jar</td><td>pesto</td></tr>\n</table></body></html>"\n# In Python 2.5 or from http://effbot.org/zone/element-index.htm\nimport xml.etree.ElementTree as etree\ntree = etree.fromstring(dinner_recipe)\n# For invalid HTML use http://effbot.org/zone/element-soup.htm\n# import ElementSoup, StringIO\n# tree = ElementSoup.parse(StringIO.StringIO(dinner_recipe))\npantry = set(['olive oil', 'pesto'])\nfor ingredient in tree.getiterator('tr'):\n\tamt, unit, item = ingredient\n\tif item.tag == "td" and item.text not in pantry:\n\t\tprint "%s: %s %s" % (item.text, amt.text, unit.text)'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      dinner_recipe = '''<html><body><table>
      <tr><th>amt</th><th>unit</th><th>item</th></tr>
      <tr><td>24</td><td>slices</td><td>baguette</td></tr>
      <tr><td>2+</td><td>tbsp</td><td>olive oil</td></tr>
      <tr><td>1</td><td>cup</td><td>tomatoes</td></tr>
      <tr><td>1</td><td>jar</td><td>pesto</td></tr>
      </table></body></html>'''
      
      import xml.etree.ElementTree as etree
      tree = etree.fromstring(dinner_recipe)
      pantry = set(['olive oil', 'pesto'])
      for ingredient in tree.getiterator('tr'):
        amt, unit, item = ingredient
        if item.tag == "td" and item.text not in pantry:
          print "%s: %s %s" % (item.text, amt.text, unit.text)


    elif (response==17):
      code='''BOARD_SIZE = 8\nclass BailOut(Exception):\n\tpass\ndef validate(queens):\n\tleft = right = col = queens[-1]\n\tfor r in reversed(queens[:-1]):\n\t\tleft, right = left-1, right+1\n\t\tif r in (left, col, right):\n\t\t\traise BailOut\ndef add_queen(queens):\n\tfor i in range(BOARD_SIZE):\n\t\ttest_queens = queens + [i]\n\t\ttry:\n\t\t\tvalidate(test_queens)\n\t\t\tif len(test_queens) == BOARD_SIZE:\n\t\t\t\treturn test_queens\n\t\t\telse:\n\t\t\t\treturn add_queen(test_queens)\n\t\texcept BailOut:\n\t\t\tpass\nraise BailOut\nqueens = add_queen([])\nprint queens\nprint "\n".join(". "*q + "Q " + ". "*(BOARD_SIZE-q-1) for q in queens)'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      BOARD_SIZE = 8

      class BailOut(Exception):
        pass

      def validate(queens):
        left = right = col = queens[-1]
        for r in reversed(queens[:-1]):
          left, right = left-1, right+1
          if r in (left, col, right):
            raise BailOut

      def add_queen(queens):
        for i in range(BOARD_SIZE):
          test_queens = queens + [i]
          try:
            validate(test_queens)
            if len(test_queens) == BOARD_SIZE:
              return test_queens
            else:
              return add_queen(test_queens)
          except BailOut:
            pass
        raise BailOut

      queens = add_queen([])
      print queens
      print "\n".join(". "*q + "Q " + ". "*(BOARD_SIZE-q-1) for q in queens)


    elif (response==18):
      code='''import random\nguesses_made = 0\nname = raw_input('Hello! What is your name?\n')\nnumber = random.randint(1, 20)\nprint 'Well, {0}, I am thinking of a number between 1 and 20.'.format(name)\nwhile guesses_made < 6:\n\tguess = int(raw_input('Take a guess: '))\n\tguesses_made += 1\n\tif guess < number:\n\t\tprint 'Your guess is too low.'\n\tif guess > number:\n\t\tprint 'Your guess is too high.'\n\tif guess == number:\n\t\tbreak\nif guess == number:\n\tprint 'Good job, {0}! You guessed my number in {1} guesses!'.format(name, guesses_made)\nelse:\n\tprint 'Nope. The number I was thinking of was {0}'.format(number)'''
      print "\nCODE: \n",code,"\n\nRESULTS: "
      import random
      guesses_made = 0
      name = raw_input('Hello! What is your name?\n')
      number = random.randint(1, 20)
      print 'Well, {0}, I am thinking of a number between 1 and 20.'.format(name)
      while guesses_made < 6:
        guess = int(raw_input('Take a guess: '))
        guesses_made += 1

        if guess < number:
          print 'Your guess is too low.'

        if guess > number:
          print 'Your guess is too high.'

        if guess == number:
          break

      if guess == number:
        print 'Good job, {0}! You guessed my number in {1} guesses!'.format(name, guesses_made)
      else:
        print 'Nope. The number I was thinking of was {0}'.format(number)


    else:
      repeat=0
    
    print ""

  spaceFunc()


def mainFunc():
	print "Hi there, this is a very basic python tutorial."
	exit=0
	while exit==0:
		response = raw_input("options:\n0) QUIT\n1) BASICS\n2) CONTROL FLOW\n3) USER INPUT\n4) STRINGS\n5) LISTS\n6) FUNCTIONS\n7) TYPE CASTING\n8) FUN EXAMPLES\n")

		response = int(response)
	
		#basics
		if response==1:
			spaceFunc()
			basicsFunc()
		
		#ctrl flow
		elif response==2:
			spaceFunc()
			ctrlFlow()

		elif response==3:
			spaceFunc()
			userInput()

		#strings
		elif response==4:
			spaceFunc()
			stringsFunc()

		#lists
		elif response==5:
			spaceFunc()
			listsFunc()

		elif response==6:
			spaceFunc()
			functions()

                elif response==7:
                        spaceFunc()
                        typecasting()

                elif response==8:
                        spaceFunc()
                        funExamples()
                #PythonInfo Wiki, Simple Programs by Steve Howell
                
		else:
			spaceFunc()
                        print "Exiting!"
			exit=1
			quitFunc()

mainFunc()
