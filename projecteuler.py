#! /usr/bin/env python
# @Programmer: Phumelela Mdluli
# @File: Ten Project Euler Solutions
# @Description: Play with this script to see some interesting number patterns. For example, we get the number 22 from the difference between the sum of squares and the square of the sum of the numbers from 1 to 3. If you ever wondered what people do with numbers when not counting money or computing their probability of winning a gamble... this could make numbers come alive for you! Will you ever look at 22 the same again? 
import array as arr

# @input: integer
# @output: integer
def sumOfDigits(D): 
  number_input = str(D)
  sum_of_digits = 0

  for i in range(len(number_input)):
      digit_at_i = int(number_input[i])
      sum_of_digits = sum_of_digits + digit_at_i
  
  return sum_of_digits

     
# @input: integer
# @output: integer
def factorial(D):
  factorial = 1
  for x in range(D,0,-1):
      factorial = factorial * x

  return factorial

 
# @input: integer
# @output: integer array
def createChain(D):
  chain = [D]
  value = chain[len(chain)-1]
  while (value!=1):
      if (value%2 == 0): # last num is even
          new_value = value/2
          chain.append(new_value)  
      else: # last num is odd
          new_value = (3*value) + 1
          chain.append(new_value)

      value = chain[len(chain)-1]

  return chain


# @input: integer
# @output: boolean
def is_palindrome(C):
  word = str(C)
  size = len(word)
  palindrome = 1
  j = size

  for i in range(size/2):
      if (word[i]!=word[j-1]):
          palindrome = 0
          break

      j = j-1
  return palindrome


# @input: integer
# @output: integer array
def pythagoreanTriplet(pt_sum):
  pt_array = []
  for a in range(1,pt_sum-2):
      for b in range(a+1,pt_sum-1):
          for c in range(b+1,pt_sum):
              if ((a<b and b<c) and ((a+b+c) == pt_sum) and ((a**2 + b**2) == c**2)):
                  pt_array.append(a)
                  pt_array.append(b)
                  pt_array.append(c)

  return pt_array


# @input: integers
# @output: integer array
def multiples(X,LIMIT):
  X=int(X)
  LIMIT=int(LIMIT)
  Multiples=arr.array('i',[])

  for num in range(X,LIMIT+1):
    REMAINDER=num%X
    if (REMAINDER==0):
      Multiples.append(num)

  return Multiples


# @input: integer array
# @output: integer array
def commonValues(A,B):
  C=arr.array('i',[])
  for i in range(0,len(B)):
    if (A.__contains__(B[i])):
      C.append(B[i])

  return C


# @input: integer array
# @output: integer
def integerArraySum(A):
  sum=0
  for i in range(0,len(A)):
    sum=sum+A[i] 

  return sum


# @input: integer
# @output: integer
def repeatPrompt(D):
  repeat = D
  prompt = raw_input("Repeat the problem? Y/N")
  if (prompt=="Y" or prompt=="y"):
      repeat = 1

  elif (prompt=="N" or prompt=="n"):
      repeat = 0

  else:
      print "Unrecognized input. Returning to main menu"

  print ""
  return repeat


def naturalSum():
    repeat = 1
    while (repeat==1):
        print "Multiple of 3 XOR 5 Sum"
        N = raw_input("Please enter a natural (positive integer) number: ")
        N = int(N) 
        sum = 0

        for V in range(N):
            if (V%3 == 0 or V%5 == 0):
                sum = sum + V

        print "The sum of the natural numbers that are multiples of 3 or (XOR) 5 below",N,"is",sum,"."
        repeat = repeatPrompt(repeat)

    main()


def evenFibonacciSum():
  repeat = 1
  while (repeat == 1):
      # Fibonacci starting with 1 & 2
      print "Even Fibonacci Number Sum" 

      n = raw_input("Please enter the fibonacci value upper-limit:")
      n = int(n)
      lastFibNum = 1
      currentFibNum = 2
      sequence = [lastFibNum]
      evenSum = 0

      if (lastFibNum%2 == 0):
          evenSum = evenSum + lasFibNum

      while (currentFibNum <= n ):
          sequence.append(currentFibNum)
          if (currentFibNum%2 == 0):
              evenSum = evenSum + currentFibNum

          nextFibNum = lastFibNum + currentFibNum
          lastFibNum = currentFibNum
          currentFibNum = nextFibNum

      print "The fibonacci sequence is:",sequence
      print "The sum of the even-valued terms, less than",n,"in the fibonacci sequence is",evenSum,"."

      repeat = repeatPrompt(repeat)
 
  main()


def primeFactor():
  repeat = 1

  while (repeat==1):
      print "Largest Prime Factor"
      N = raw_input("Please enter a natural number (positive integer):")
      N = int(N)
      factors = []
      primefactors = []

      for X in range(1,N+1):
          if (N%X == 0): # X is a factor
              factors.append(X)
              if (X==1 or X==2):
                  primefactors.append(X)
          
              if (X>2 and X%2!=0):
                  # X is odd, check if X is prime
                  is_prime = 1

                  for i in range(2,X): # can be optimized by cutting down the search halfway!!
                     if (X%i==0): #X has a factor and is not prime
                       is_prime = 0
                       break

                  if (is_prime):
                      primefactors.append(X)


      print "The prime factors of",N,"are:",primefactors
      print "The factors of",N,"are:",factors
      print "The largest prime factor of",N,"is",primefactors[len(primefactors)-1],"."

      repeat = repeatPrompt(repeat)

  main()


def palindromeNumber():
  repeat = 1
  
  while (repeat==1):
      print "Palindrome Number"
      N_digit = raw_input("How many digits do you want the multipliers to have (e.g. a 2-digit number is 50)?")
      N_digit = int(N_digit)
      product = 0
      limit = 10**N_digit
      exit_loop = 0
      palindrome_limit = (limit-1)*(limit-1)

      print "If the largest product of two",N_digit,"-digit numbers is",palindrome_limit,"."
      p_limit = raw_input("Please enter the palindrome limit (enter 0 to use the largest product)?")
      p_limit = int(p_limit)
      if (p_limit!=0):
        palindrome_limit = p_limit

      for A in range(limit-1,0,-1):
          if (exit_loop):
              break
          for B in range(limit-1,0,-1):
             if (exit_loop):
                 break

             product = A * B
             if (is_palindrome(product) and product <= palindrome_limit):
                 print product
                 exit_loop = 1

      print "The largest palindrome made from the product of two",N_digit,"-digit numbers less than",palindrome_limit,"is",product,".\n"

      repeat = repeatPrompt(repeat)

  main()


def positiveDiv():
  repeat = 1
  
  while (repeat==1):
      print "Even Divisibility"
      N = 10
      INT_ = 2520
      print "The smallest positive number divisible by all the integers 1 to",N,"is",INT_,"."
      N = raw_input("Please enter a number N to find the smallest natural number divisible by the integers from 1 to N:")
      N = int(N)
      found = 0
      INT_ = 0
      while (found == 0):
          INT_ = INT_ + 1
          for i in range(1,N+1):
              if (INT_%i != 0):
                found=0
                break

              if (i==N):
                found = 1
                break
            
      print "The smallest natural number divisible by the integers 1 to",N,"(without any remainder) is",INT_,"."

      repeat = repeatPrompt(repeat)

  main()


def sumOfSquares():
  repeat = 1
  
  while (repeat==1):
      print "Difference of Sums"
      N = raw_input("Enter a number N to find the difference between the square of the sum and the sum of the squares, from 1 to N:")
      N = int(N)
      sum_of_squares = 0
      square_of_sum = 0

      for i in range(1,N+1):
          square = int(i)**2
          sum_of_squares = sum_of_squares + square
          square_of_sum = square_of_sum + int(i)

      square_of_sum = square_of_sum**2
      square_difference = square_of_sum - sum_of_squares

      print "For the first",N,"natural numbers, the sum of the squares is",sum_of_squares,", the square of the sum is",square_of_sum,", and the difference is",square_difference,"."

      repeat = repeatPrompt(repeat)

  main()


def nthPrime():
  repeat = 1
  
  while (repeat==1):
      print "Nth Prime"
      N = raw_input("Please enter a number N to find the N-th prime (primes not including 1):")
      N = int(N)
      primes = [2]
      X = primes[len(primes)-1] + 1

      while (len(primes)!=N):
          if (X%2!=0 and X>2): # X is odd
              is_prime = 1

              for i in range(2,X):
                  if (X%i==0): #X has a factor and is not prime
                      is_prime = 0
                      break

              if (is_prime):
                  primes.append(X)

          X = X + 1

      print "Primes:",primes 
      print "The",N,"st/nd/rd/th prime is",primes[N-1],"."
      
      repeat = repeatPrompt(repeat)

  main()


def pythagoreanTripletProduct():
  # NB: numbers above 1000 take longer to compute
  repeat = 1
  pt_sum = 1000
  while (repeat==1):
      product = 0
      print "Pythagorean Triplet (PT): is a set of three natural numbers a,b,c such that a^2 + b^2 = c^2. There might exist a PT such that a + b + c =",pt_sum,"." 
      pt_array = pythagoreanTriplet(pt_sum)
      if (len(pt_array) == 0):
          print "A solution does not exists for a + b + c =",pt_sum,"."

      else:
          a = pt_array[0]
          b = pt_array[1]
          c = pt_array[2]
     
          product = a*b*c
          print "A:",a,", B:",b,", C:",c
          print "ABC:",product

      repeat = repeatPrompt(repeat)
      if (repeat==1):
          pt_sum = raw_input("Please enter a new sum:")
          pt_sum = int(pt_sum)

  main()


def longestChain():
  repeat = 1
  
  while (repeat==1):
      print "Longest Chain"
      limit = raw_input("Please enter a limit (enter 0 to use 1million):")
      limit = int(limit)

      if (limit ==0):
          limit = 1000000

      longest = 0
      start_num = 0
      x_chain = []

      for x in range(1,limit):
          chain = createChain(x)
          if (len(chain)>longest):
             longest = len(chain)
             start_num = x
             x_chain = chain
             
      print "The longest chain under",limit,"occurs when the starting number is",start_num,"and is of size",longest,"."
      print "Chain:",x_chain

      repeat = repeatPrompt(repeat)

  main()


def sumOfPowerDigits():
  repeat = 1
  
  while (repeat==1):
      print "Sum of Power Digits"
      exponent = raw_input("Please enter an exponent of 2 (enter 0 to use 1000):")
      exponent = int(exponent)

      if (exponent == 0):
         exponent = 1000

      power = 2**exponent
      sum_of_digits = sumOfDigits(power)

      print "2^",exponent,"=",power,". The sum of the digits of",power,"is",sum_of_digits,"."
      repeat = repeatPrompt(repeat)

  main()


def sumOfDigitsFactorial():
  repeat = 1
  
  while (repeat==1):
      print "Sum of Factorial Digits"
      N = raw_input("Please enter a natural number (enter 0 to use 100):")
      N = int(N)

      if (N == 0):
         N = 100

      fact = factorial(N)
      sum_of_digits = sumOfDigits(factorial)
      print N,"!=",fact,". The sum of the digits of",fact,"is",sum_of_digits,"."

      repeat = repeatPrompt(repeat)

  main()


def sumOfMultiples():
  repeat=1
  while (repeat==1):
    print "Sum of multiples of X AND Y"
    x=raw_input("Please enter an integer X: ")
    x=int(x)
    y=raw_input("Please enter an integer Y: ")
    y=int(y)
    l=raw_input("Please enter an upper limit for the multiple search: ")
    l=int(l)

    XMultiples=multiples(x,l)
    YMultiples=multiples(y,l)
    
    XYMultiples=commonValues(XMultiples,YMultiples)
    MultipleSum=integerArraySum(XYMultiples)

    print "The sum of the multiples of",x,"AND",y,"is",MultipleSum,"."

    repeat=repeatPrompt(repeat)

  main()


def solutions(n):
  print " *** Project Euler Solution *** "
  if (n==1):
    naturalSum()
 
  elif (n==2):
    evenFibonacciSum()

  elif (n==3):
    primeFactor()
 
  elif (n==4):
    palindromeNumber() 
  
  elif (n==5):
    positiveDiv()

  elif (n==6):
    sumOfSquares()

  elif (n==7):
    nthPrime()

  elif (n==8):
    pythagoreanTripletProduct()
 
  elif (n==9):
    longestChain()
  
  elif (n==10):
    sumOfPowerDigits()

  elif (n==11):
    sumOfDigitsFactorial()

  elif (n==12):
    sumOfMultiples()

  else:
    print "Unexpected exit!"
    quit()

# -------------------------------------
def questions():
    print " *** Project Euler Questions *** "
    print "\n1) If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3,5,6 and 9. The sum of these multiples is 23. Find the sum of all the multiples of 3 or 5 below N."
    print "\n2) Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be: 1,2,3,5,8,13,21,34,55,89,.. By considering the terms in the Fibonacci sequence whose values do not exceed n, find the sum of the even-valued terms. \ne.g. for n=10, we have {2,8}, sum is 10."
    print "\n3) The prime factors of 13195 are 5,7,13 and 29. What is the largest prime factor of a given number N? e.g. for 10, largest prime factor = 5. For 17, largest prime factor = 17."
    print "\n4) A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009, 99 * 91. Find the largest palindrome made from the product of two 3-digit numbers which is less than N."
    print "\n5) 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder. What is the smallest positive number that is evenly divisible(divisible with no remainder) by all of the numbers from 1 to N?"
    print "\n6) The sum of the squares of the first ten natural numbers is 1**2 + 2**2 + ... + 10**2 = 385. The square of the sum of the first ten natural numbers is (1 + 2 + ... + 10)**2 = 552 = 3025. Hence, the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 - 385 = 2640. Find the difference between the sum of the squares of the first N natural numbers and the square of the sum."
    print "\n7) By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13. What is the Nth prime number?"
    print "\n8) A Pythagorean triplet is a set of three natural numbers, a < b < c, for which, a^2 + b^2 = c^2 There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc."
    print "\n9) The following iterative sequence is defined for the set of positive integers: n-> n/2 (n is even) n-> 3n + 1 (n is odd). Using the rule above and starting with 13, we generate the following sequence: 13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1. Which starting number, under one million, produces the longest chain?"
    print "\n10) 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26. What is the sum of the digits of the number 2^1000?"
    print "\n11) n! means n * (n-1) * ... * 3 * 2 * 1. For example, 10! = 10 * 9 * ... * 3 * 2 * 1 = 3 628 800, and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27. Find the sum of the digits in the number 100!"
    print "\n12) Given two integers, X and Y, find the sum of the multiples. e.g. given 15 and 35, the multiples are (15, 30, 45, 60, and 90) and (35, and 70). Thus, the sum of the multiples of 15 AND 35 is 0."
    print "\n13) EXIT!"


def main():
    questions()
    sol = raw_input("\n\nWhich solution would you like to view?")
    sol = int(sol)

    if (sol>0 and sol<13):
        solutions(sol)
    else:
        print "Bye!"
        quit()

main()
